














                                          GenREF

                                          v1.00



                                  MDOS Reference guide.

























                                    (C) Copyright 1989

                                     J. Paul Charlton

                                   ALL RIGHTS RESERVED















               [1m____________________________________________________________[0m
                                    [1mUTILITY ‐ CONTENTS[0m
               [1m____________________________________________________________[0m

                                                                 Page



               Utility Overview..................................1

               Calling Utility Functions.........................1


               Validate Time.....................................2

               Read Time.........................................3

               Set Time..........................................4

               Validate Date.....................................5

               Read Date.........................................6

               Set Date..........................................7

               Julian Date.......................................8

               Day of Week.......................................9

               Parse Filename....................................10

               Load Task.........................................13

               Fork Task.........................................14



































               [1m____________________________________________________________[0m
                                     [1mUTILITY OVERVIEW[0m
               [1m____________________________________________________________[0m

                  The  memory  management  routines in MDOS are provided to
               aid a programmer in writing applications  which  are  larger
               than  the 64 Kbytes directly addressable by the CPU’s 16 ad‐
               dress lines.  They also serve the purpose of providing  each
               task with it’s own private address space, separate from oth‐
               er the memory accessible to other tasks.


               [1m____________________________________________________________[0m
                                [1mCALLING UTILITY FUNCTIONS[0m
               [1m____________________________________________________________[0m

                  The MDOS utility functions must be called from  within  a
               machine code program running as a task under MDOS.  You pass
               arguments to the utility functions using only a  few  regis‐
               ters of your program’s workspace.

                  The  MDOS  utility  functions  are invoked from a machine
               code program when software  trap  number  zero  (XOP  0)  is
               called with a library number of 9.  The calling program’s R0
               must contain the opcode of the routine  within  the  utility
               library  which is to be performed.  The following code frag‐
               ment will return the day of the week to the calling task.

                         LI        R0,7
                         XOP       @NINE,0
                         MOV       R1,@WEEKDA
               * ...
               WEEKDA    DATA      0         day of the week (1‐7):(Sun‐Sat)
               * ...
               NINE      DATA      9
               * ...





























          UTIL ‐ 2                GenREF v1.00


          [1m____________________________________________________________[0m
                                 [1mVALIDATE TIME[0m
          [1m____________________________________________________________[0m

          [1mFunction  [22mThis operation is used to check the time stored in
                    the  clock chip for validity.  It insures that the
                    minutes and seconds are in the range 0:59, and in‐
                    sures that the hours are in the range 0:23.

          [1mParameters[22mR0        = 0 (opcode)

          [1mResults   [22mEQ status

          [1mParameter description[0m

          EQ status The  equal  status  bit will be set if the time is
                    valid, allowing you to  perform  a  "JEQ  time$ok"
                    right after the software trap.













































                                       GenREF v1.00                UTIL ‐ 3


               [1m____________________________________________________________[0m
                                        [1mREAD TIME[0m
               [1m____________________________________________________________[0m

               [1mFunction  [22mThis  operation  reads  the  time  of day from the
                         clock chip, and places it into your string  buffer
                         as  a  formatted  string,  with colons between the
                         hours, minutes, and seconds.

               [1mParameters[22mR0        = 1 (opcode)
                         R1        = buffer

               [1mResults   [22mBuffer contains time string "HH:MM:SS".

               [1mParameter description[0m

               Buffer    The buffer address you pass for the  string  is  a
                         16‐bit  address  within your task’s linear address
                         space.  The buffer must be  ten  characters  long,
                         and  the  address  you  pass is the address of the
                         second character in the buffer.

                         On return, the first character of the buffer (off‐
                         set 0) will contain a length byte.  The next eight
                         characters, starting at the address you specified,
                         will  contain the formatted time string.  The last
                         character in the buffer (offset 9) will contain  a
                         zero byte, for a null terminated string.



































          UTIL ‐ 4                GenREF v1.00


          [1m____________________________________________________________[0m
                                    [1mSET TIME[0m
          [1m____________________________________________________________[0m

          [1mFunction  [22mThis  operation  will set the clock chip using the
                    time in the formatting string  which  the  calling
                    task passes as an argument.


          [1mParameters[22mR0        = 2 (opcode)
                    R1        = string

          [1mResults   [22mEQ status

          [1mParameter description[0m

          string    The  address  you  pass for the string is a 16‐bit
                    address within your task’s linear  address  space.
                    The  address you pass is the address of the second
                    character in the buffer (the first text  character
                    in the string.)

                    The first character in the string buffer must be a
                    length byte, giving the number of text  characters
                    in  the  string.  Any leading spaces in the string
                    will be ignored.

                    The text of the string  must  have  the  following
                    format:

                    [h]h:[m]m[:[s][s]]

          EQ status The  equal  status  bit  will  be  set if the time
                    string is valid, allowing you to  perform  a  "JEQ
                    time$ok" right after the software trap.  The clock
                    chip is not altered unless the EQ status has  been
                    returned.


























                                       GenREF v1.00                UTIL ‐ 5


               [1m____________________________________________________________[0m
                                      [1mVALIDATE DATE[0m
               [1m____________________________________________________________[0m

               [1mFunction  [22mThis operation is used to check the date stored in
                         the clock chip for validity.  It insures that  the
                         month  is  in the range 1:12, the day of the month
                         is the range 1:MAX_DAYS[month], the year is in the
                         range  0:99, and that the day of the week based on
                         the month‐day‐year in the clock chip  agrees  with
                         the  day  of the week stored in the clock chip it‐
                         self.

               [1mParameters[22mR0        = 3 (opcode)

               [1mResults   [22mEQ status

               [1mParameter description[0m

               EQ status The equal status bit will be set if  the  date  is
                         valid,  allowing  you  to  perform a "JEQ date$ok"
                         right after the software trap.









































          UTIL ‐ 6                GenREF v1.00


          [1m____________________________________________________________[0m
                                   [1mREAD DATE[0m
          [1m____________________________________________________________[0m

          [1mFunction  [22mThis operation reads the date from the clock chip,
                    and places it into your string buffer as a format‐
                    ted string, with a dash between  the  month,  day,
                    and year.

          [1mParameters[22mR0        = 4 (opcode)
                    R1        = buffer

          [1mResults   [22mBuffer contains date string "mm‐dd‐yy".

          [1mParameter description[0m

          Buffer    The  buffer  address  you pass for the string is a
                    16‐bit address within your task’s  linear  address
                    space.   The  buffer  must be ten characters long,
                    and the address you pass is  the  address  of  the
                    second character in the buffer.

                    On return, the first character of the buffer (off‐
                    set 0) will contain a length byte.  The next eight
                    characters, starting at the address you specified,
                    will contain the formatted date string "mm‐dd‐yy".
                    The  last  character in the buffer (offset 9) will
                    contain a zero byte, for a null terminated string.



































                                       GenREF v1.00                UTIL ‐ 7


               [1m____________________________________________________________[0m
                                         [1mSET DATE[0m
               [1m____________________________________________________________[0m

               [1mFunction  [22mThis operation will set the clock chip  using  the
                         date  in  the  formatting string which the calling
                         task passes as an argument.


               [1mParameters[22mR0        = 5 (opcode)
                         R1        = string

               [1mResults   [22mEQ status

               [1mParameter description[0m

               string    The address you pass for the string  is  a  16‐bit
                         address  within  your task’s linear address space.
                         The address you pass is the address of the  second
                         character  in the buffer (the first text character
                         in the string.)

                         The first character in the string buffer must be a
                         length  byte, giving the number of text characters
                         in the string.  Any leading spaces in  the  string
                         will be ignored.

                         The  text  of  the  string must have the following
                         format:

                         [m]m/[d]d[/[y][y]] [m]m‐[d]d[‐[y][y]]

               EQ status The equal status bit  will  be  set  if  the  date
                         string  is  valid,  allowing you to perform a "JEQ
                         date$ok" right after the software trap.  The clock
                         chip  is not altered unless the EQ status has been
                         returned.


























          UTIL ‐ 8                GenREF v1.00


          [1m____________________________________________________________[0m
                                  [1mJULIAN DATE[0m
          [1m____________________________________________________________[0m

          [1mFunction  [22mThis operation performs the function of a perpetu‐
                    al calendar, and will work on any date after Janu‐
                    ary 1st, 1 AD.


          [1mParameters[22mR0        = 6 (opcode)
                    R1        = month
                    R2        = day
                    R3        = year

          [1mResults   [22mR1,R2     = julian date

          [1mParameter description[0m

          Year      This must be the full year, like "1989", not "89",
                    for the year in which this documentation was writ‐
                    ten.

          Julian dateThis is the number of days since January 1st, 4712
                    B.C.







































                                       GenREF v1.00                UTIL ‐ 9


               [1m____________________________________________________________[0m
                                       [1mDAY OF WEEK[0m
               [1m____________________________________________________________[0m

               [1mFunction  [22mReturns  the day of the week, from one (Sunday) to
                         seven (Saturday).


               [1mParameters[22mR0        = 7 (opcode)

               [1mResults   [22mR1        = weekday

               [1mParameter description[0m

               Weekday   This is a sixteen bit integer with  a  value  from
                         >0001 (Sunday) to >0007 (Saturday).















































          UTIL ‐ 10               GenREF v1.00


          [1m____________________________________________________________[0m
                                 [1mPARSE FILENAME[0m
          [1m____________________________________________________________[0m

          [1mFunction  [22mThis operation will convert a logical filename de‐
                    scriptor to a physical filename descriptor  recog‐
                    nized  by  the  Device Service Routines.  For disk
                    devices, the conversion may depend  on  the  drive
                    currently  set for the task and the current subdi‐
                    rectory on the drive (depending on  the  ambiguity
                    left in the name by the calling program.)

                    It  is  useful when you wish to make your applica‐
                    tion program independent of which  device  it  was
                    loaded  from  or  when your application must ask a
                    user for a filename.

          [1mParameters[22mR0        = 8 (opcode)
                    R1        = logical name
                    R2        = physical name
                    R3        = alias flag


          [1mResults   [22mR0        = delimiter
                    R1        = error code
                    EQ status


          [1mParameter description[0m

          Logical namTehis is the address of the first character in  the
                    string to be converted to a physical device name.

                    At first, the name is compared to the names of all
                    character devices  recognized  by  MDOS.   If  the
                    string  matches  the  name of any of the character
                    devices,  the string will be copied without  modi‐
                    fication to the specified string output buffer.

                    There  are three separators regonized by this rou‐
                    tine as part of a disk path name: COLON ":", PERI‐
                    OD ".", and BACKSLASH "\".  If the first separator
                    found before a terminating delimiter is a  PERIOD,
                    the entire string will be copied without modifica‐
                    tion to the specified string output buffer.

                    The following characters indicate the end  of  the
                    input  string  if they are not contained inside of
                    double‐quote marks:  SPACE,  COMMA,  SLASH,  SEMI‐
                    COLON.   The NIL character (>00) always terminates
                    the input string, even if there are unmatched dou‐
                    ble  quotes  in  the string.  These characters are
                    referred to as "terminal characters".










                                       GenREF v1.00               UTIL ‐ 11

                         The terminal characters along with the three sepa‐
                         rator characters are known as "delimiters".

                         Remaining filenames are parsed as follows:

                         Part  A, drive alias. All characters parsed during
                         this phase are ignored in subsequent phases of the
                         parsing.

                         caller R3 alias flag <>0      ‐> null string
                         "volume:" + non‐terminal      ‐> "WDS.volume."
                         "volume:" + terminal character          ‐> "volume"
                         "n:"      + terminal character          ‐> "alias"
                         "n:"      + non‐terminal, "\", or "."   ‐> "alias."
                         all others                              ‐> "alias."

                         Part  B,  current directory.  Using the characters
                         remaining in the input string after Part A.

                         "\"       is first character  ‐> null string
                         current dir         is NULL             ‐> null string
                         "."       + terminator                  ‐> "CURDIR"
                         ".."      + terminator                  ‐> "PARENTDIR"
                         ".\"      + non‐delimiters    ‐> "CURDIR."
                         "..\\"    + non‐delimiters    ‐> "PARENTDIR."
                         all others                              ‐> "CURDIR."

                         Part C, file specifier.  This  is  the  characters
                         remaining  in  the  string after Part A and Part B
                         have been done.  These characters are copied  into
                         the  output  buffer  until  a BACKSLASH, QUOTE, or
                         terminator is found.  When a BACKSLASH  is  found,
                         it  is replaced by a PERIOD.  When a terminator is
                         found, parsing of the input string is stopped, and
                         the  address  of the terminator is returned to the
                         caller.  When a QUOTE is found, all characters un‐
                         til  the next QUOTE or NIL in the input string are
                         copied to the output buffer (Unless  two  matching
                         QUOTES  were adjacent to each other, in which case
                         a single QUOTE character will be placed  into  the
                         output buffer.)

                         The resulting string in the output buffer is "PART
                         A" + "PART B" + "PART C", and is returned  to  the
                         caller.



















          UTIL ‐ 12               GenREF v1.00

          Phys. nameThis  is  most  useful  when it specifies the name
                    length byte in a PAB.

                    Note that the physical name can use the same  buf‐
                    fer as the logical name, and will simply overwrite
                    the logical name after parsing is complete.  As  a
                    caller, you must specify the address of the length
                    byte in your string output buffer with this param‐
                    eter.   Before calling the parse routine, you must
                    set the length byte to the maximum length  allowed
                    for  the  output  string, which is returned in the
                    form "<len><chars><nil>".

          Alias flagThis flag must be set to zero for normal  process‐
                    ing.   If  this  flag  is  non‐zero, no disk drive
                    alias will be prepended to  the  output  filename.
                    (This feature is used only by the CHDIR command of
                    MDOS at present.)

          Delimiter This is the address of the first character in your
                    input  string  which  wasn’t  processed during the
                    generation of the filename.  Note that this is de‐
                    signed  in such a fashion that you generally don’t
                    need your own routine to parse filenames which are
                    passed to your program as command line parameters;
                    just call the parse routine, check the  delimiter,
                    and call the parse routine again for the next com‐
                    mand line parameter.

          Error codeThis is set to zero if no errors were  encountered
                    while  parsing the filename.  This is non‐zero un‐
                    der  several  conditions:  the  resulting   output
                    string  was  too  long for your buffer, a COLON is
                    the first character in the input string,  a  drive
                    specified  with  "n:"  does  not  have an assigned
                    alias, or a directory specifier of the form "." or
                    ".." was not followed by a BACKSLASH or a termina‐
                    tor.


























                                       GenREF v1.00               UTIL ‐ 13


               [1m____________________________________________________________[0m
                                        [1mLOAD TASK[0m
               [1m____________________________________________________________[0m

               [1mFunction  [22mThis operation will load a chained  program  image
                         file  into  memory,  and  cause it to execute as a
                         task under MDOS.  Invocation of the new task  will
                         start  at address >0400 with a workspace of >F000,
                         and memory windows 0..6 of the task will initially
                         contain the data loaded from the program image.

               [1mParameters[22mR0        = 9 (opcode)
                         R1        = physical name

               [1mResults   [22mR0        = error code
                         R1        = child page zero (physical page number)

                         >00E8     in child task contains the physical page number
                                   of the parent’s page zero

               [1mParameter description[0m

               Phys. nameThis  is the address of the length byte of a file‐
                         name stored in the format "<len><chars>".

               Error code
                         0         = no error, task was loaded
                         1         = insufficient memory
                         2         = invalid filename
                         3         = image file found, with invalid header

               [1mImage file header[0m

                         An image file header  has  the  following  format,
                         compatible with GenLINK.

                         byte 0    if >00, last image in chain, otherwise bump
                                   filename and load another image in the chain

                         byte 1    "G" (normal speed) or "F" (use fast memory)

                         byte 2,3  length of this image file

                         byte 4,5  load address of this image file

                         byte 6..len+6       image data bytes

















          UTIL ‐ 14               GenREF v1.00


          [1m____________________________________________________________[0m
                                   [1mFORK TASK[0m
          [1m____________________________________________________________[0m

          [1mFunction  [22mThis  operation  causes the creation of a new task
                    under MDOS.  The new task (child task) is an exact
                    copy of the calling task (parent task).

                    The  new  task  has a virtual memory address space
                    which has one physical memory page for each physi‐
                    cal  page  used by the parent task.  If the parent
                    task was using shared memory, the child  task  can
                    also  use  the  same shared memory and communicate
                    with the parent task.

                    Further note:  The terms "parent" and "child"  are
                    used  as  a convenience in differentiating between
                    the calling task and the newly created  task.   In
                    MDOS itself, there is no concept of "task tree" or
                    parent‐child relationship as there is in some oth‐
                    er  operating  systems.   In  MDOS,  all tasks are
                    peers.


          [1mParameters[22mR0        = 10 (opcode)

          [1mResults   [22mparent task:

                    R0        = ‐1 (error)
                              = otherwise, this is the physical page number of the
                                child task’s header page.

                    PC        = program execution continues with the instruction after the
                                XOP call.  This instruction must be a single word instruction
                                such as a Jump instruction.

                    child task:

                    R0        = ‐1

                    PC        = program execution continues with the 2nd instruction after
                                the XOP call.  Note that the instruction after the XOP call
                                must be a single word instruction.

                    >00E8     in child task contains the physical page number
                              of the parent’s page zero

















                                       GenREF v1.00               UTIL ‐ 15

               [1mParameter description[0m

               error     An error code of ‐1 will be returned to the parent
                         task  if  there  is not enough memory available on
                         your system to create a clone of your task.


               [1mExample code[0m
                                   LI        R0,10
                                   XOP       @NINE,0
                                   JMP       PARENT
                         CHILD     PRINT     "This is the child speaking..."

















































