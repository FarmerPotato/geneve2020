
# Common files
VERILOG_MODULES = ../debounce.v ../frame_buffer.v

all: debounce_tb frame_buffer_tb

debounce_tb: debounce_tb.v

frame_buffer_tb: frame_buffer_tb.v





# how to add prequisite $(VERILOG_MODULES)?

% : %.v $(VERILOG_MODULES)
	iverilog -o $@ $^ 
	vvp $@
