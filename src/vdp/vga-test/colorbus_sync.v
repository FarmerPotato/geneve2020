// Read 9958 sync signals, clock, and colorbus

// p_tick is raised when pixel on colorbus should be latched.
// h_end, v_end are raised to indicate that the next pixel will start a new line or frame.	
// this module will wait through the blank parts and clock p_tick on display pixels,
// which are up to 512x212, but usually 256x192.
// in interlace mode, expect every one frame to be different, for 424 lines.

// From V9938 datasheet:
//                        DLCLK      DHCLK 
// frequency              10.74       5.37    MHz
// low-level pulse width   20          60     ns
// fall time               15          25     ns
// DHCLK-DLCLK delay time -15          15     ns
// 
// CL = 50pF
// RL = 1K
//                      Min Typ Max
// BLEO low                     0.4  V    blanking interval
// BLEO mid             2.5     3.5       first field, active
// BLEO high            4.5               second field, active
//
// BLEO low is ok for detecting blank. 
// LM1881 will output even/odd
//
// HSYNC fall time              110  ns
// HSYNC pulse width    4.5     4.7  us (4500 ns)        
// HSYNC rise time               90  ns
// Left border width    2.4     2.7  us
// Active display area 47   47.68  48  us
// Right border width   2.5     2.8  us
// Right border-HSYNC   1.2     1.5  us
//
// Left-Right can be adjusted!
//
// Want HYSNC mode = 0 for vertical pulses
//
// Colorbus output should be latched on rising DLCLK*
//
//
//


module vdp_sync
	(
	input wire clk, reset,
	input wire dlclk, dhclk, 
  input wire hsync, vsync,    // from hardware LM1881 or Schmidt trigger buffer
//  input wire bleo, // is this even ttl? do they need separation from tri-level?
  input blank, odd_even,
	output wire h_end, v_end, p_tick
	);

	// investigate bleo:
	// make a module that counts ticks per second (debounced).
	// use a/d pmod if necessary to characterize.
	// must separate out blank from even/odd in hardware?


	// how to get blanking and even/odd signal from BLEO?

  // how to identify porch, blanking from the V9958?

	// constant declaration - are these useful here?
	// VDP 512 x 262.5 parameters (two 1/60 frames in 525 lines)
	// timing is determined by the V9958, we're just following along.

	localparam HD = 512; // horizontal display area 
	localparam HF =  48;  // h. front porch (left) border
	localparam HB =  16;  // h. back porch (right) border
	localparam HR =  96;  // h. retrace

	localparam VD = 212; // vertical display area
	localparam VF =  33;  // v. front porch (top) border    
	localparam VB =  10;  // v. back porch (bottom) border   
	localparam VR = 2;   // v. retrace                       

	
	// count the number of events from 9958
	reg [9:0] h_count_reg, h_count_next;
	reg [9:0] v_count_reg, v_count_next;

	// output buffer
	reg  v_sync_reg, h_sync_reg;
	wire v_sync_next, h_sync_next;

	// status signal
	wire h_end, v_end, pixel_tick;

  // must debounce the TTL input
  // at 100 MHz, rise time of 20ns is 2 clocks
  
  
  

	// body
	// registers
	always @(posedge clk, posedge reset)
		if (reset)
			begin
				p_tick_reg    <= 0;
				v_count_reg <= 0;
				h_count_reg <= 0;
				v_sync_reg  <= 1'b0;
				h_sync_reg  <= 1'b0;
			end
		else
			begin
				p_tick_reg    <= p_tick_next;
				v_count_reg <= v_count_next;
				h_count_reg <= h_count_next;
				v_sync_reg  <= v_sync_next;
				h_sync_reg  <= h_sync_next;
			end
			
	always @(posedge clk) begin
		
	end

  // End detector - what should it be? 
  // how many dlclks to first display pixel?
  // 
  assign h_end = (h_count_reg == 255);
  assign v_end = (v_count_reg == 192);
  
  // h_end
  // v_end
  
  // H counter counts display or blank pixels (dlclk?)
	always @(posedge dlclk) begin
			if (h_end)
				h_count_next = 0;
			else
				h_count_next = h_count_reg + 1;		
	end

  // V counter counts display or blank lines
  // clarify what signals last display line or last frame line
	always @(posedge hsync) begin
			if (v_end)
				v_count_next = 0;
			else
				v_count_next = v_count_reg + 1;		
	end
	
	always @(dlclk, p_blank) begin
		p_tick_next = dlclk & p_blank;

    // when h/l modes are supported, 
    // extend to support dhclk
    // 256 = dlclk  5.368 MHz
    // 512 = dhclk 10.738 MHz
    // p_tick_next = dhclk & blank;

	end	

	// output
	assign hsync   = h_sync_reg;
	assign vsync   = v_sync_reg;
	assign pixel_x = h_count_reg;
	assign pixel_y = v_count_reg;
	assign p_tick  = pixel_tick;
	
endmodule
