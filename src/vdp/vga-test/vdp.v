module vdp
	(
	input wire clk, reset,
	output wire hsync, vsync,
	output wire [11:0] rgb
	);
	
	// signal declaration
	//reg [11:0] border1_reg, border2_reg, screen_reg;
	reg [3:0] border1_reg, border2_reg, screen_reg;
	wire [11:0] rgb_reg;
	wire video_on, pixel_tick;
	wire [9:0] pixel_x, pixel_y;
	
	// instantiate vga sync circuit
	vga_sync vsync_unit
		(.clk(clk), .reset(reset), .hsync(hsync), .vsync(vsync),
		.video_on(video_on), .p_tick(pixel_tick), .pixel_x(pixel_x), .pixel_y(pixel_y));
	
	// TODO
	// colorbus_sync
	// colorbus
	
	graphic graphic_unit(
	  .clk(clk),
	  .reset(reset),
	  .pixel_tick(pixel_tick),
		.pixel_x(pixel_x), 
		.pixel_y(pixel_y), 
		.border1(border1_reg), 
		.border2(border2_reg), 
		.screen(screen_reg),
		.rgb(rgb_reg)
	);
	
	// rgb buffer
 	always @(posedge clk, posedge reset)
		if (reset)
			begin
				border1_reg = 4'he; // gray
				border2_reg = 4'h7; // cyan
				screen_reg  = 4'h1; // black
			end
	// output
	assign rgb = (video_on) ? rgb_reg : 0;
	
endmodule
