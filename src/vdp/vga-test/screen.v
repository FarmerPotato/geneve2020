// Supply the pixel values
// Initially locally generated, eventually read from colorbus
// Plus overlay of local text.
module screen#(
	parameter BIT_DEPTH = 4,
	parameter SCREEN_WIDTH = 256,
	parameter SCREEN_HEIGHT = 192,
	parameter RGB_DEPTH = 12  // for output
) (
  input wire clk,
  input wire reset,
  input wire pixel_tick,
	input wire [9:0] pixel_x, pixel_y,
	input wire [BIT_DEPTH-1:0] border1,
	input wire [BIT_DEPTH-1:0] border2,
	input wire [BIT_DEPTH-1:0] screen,
	output reg [RGB_DEPTH-1:0] rgb
	);

