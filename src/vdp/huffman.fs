( huffman decompression using nodes that have 2 cells, left and right. when left==right it is a leaf. )

0 variable nodetable ( address of nodetable )
0 variable addr ( address of bitmap data )
0 variable curr ( index of current node )

: 2cells@ ( -- n1 n2 ) curr @ cells nodetable + d@ ;
: ?leaf   ( n1 n2 -- n1 n2 f ) 2dup = ;
: left    ( n1 n2 -- ) drop curr ! ;
: right   ( n1 n2 -- ) curr ! drop ;
: c@next   ( addr -- n ) 1 over +! c@ ; ( fetch byte and increment addr )
: output  ( n -- ) emit ; ( do something with byte )

: huffman_dec ( cnt addr -- )
  addr !
  0 curr !
  begin
    addr c@next ( c )
    8 0 do
      dup  
      2cells@  ( c c n1 n2 )
      ?leaf
      if output 2drop 0 curr !
        else rot 1 and 0=
          if left else right then
      then        
      2/ ( cnt c )
    loop  drop
    1-
  ?dup until
;

