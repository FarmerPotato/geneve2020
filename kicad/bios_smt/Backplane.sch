EESchema Schematic File Version 4
LIBS:BIOS_smt-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4500 2100 0    50   Input ~ 0
+5V
Text GLabel 5650 4100 0    50   Input ~ 0
B_INTREQ*
Text GLabel 4500 5200 0    50   Input ~ 0
GND
Text GLabel 5650 4000 0    50   Input ~ 0
B_NMI*
Text GLabel 4500 2800 0    50   Input ~ 0
B_D2
Text GLabel 4500 3000 0    50   Input ~ 0
B_D4
Text GLabel 4500 3200 0    50   Input ~ 0
B_D6
Text GLabel 4500 2600 0    50   Input ~ 0
B_D0_OUT
Text GLabel 3350 4800 0    50   Input ~ 0
B_ALATCH
Text GLabel 5650 5200 0    50   Input ~ 0
GND
Text GLabel 5650 2100 0    50   Input ~ 0
+5V
Text GLabel 5650 5100 0    50   Input ~ 0
B_RESET*
Text GLabel 3350 4500 0    50   Input ~ 0
B_BST2
Text GLabel 4500 3300 0    50   Input ~ 0
B_D7
Text GLabel 4500 3100 0    50   Input ~ 0
B_D5
Text GLabel 4500 2900 0    50   Input ~ 0
B_D3
Text GLabel 4500 2700 0    50   Input ~ 0
B_D1
Text GLabel 3350 2900 0    50   Input ~ 0
B_A6
Text GLabel 5650 2800 0    50   Input ~ 0
B_A8
Text GLabel 3350 3800 0    50   Input ~ 0
B_A14
Text GLabel 5650 4700 0    50   Input ~ 0
B_A12
Text GLabel 3350 2700 0    50   Input ~ 0
B_A4
Text GLabel 3350 2600 0    50   Input ~ 0
B_A2
Text GLabel 3350 4900 0    50   Input ~ 0
B_A13
Text GLabel 5650 2900 0    50   Input ~ 0
B_A7
Text GLabel 3350 5000 0    50   Input ~ 0
B_A9
Text GLabel 3350 2800 0    50   Input ~ 0
B_A5
Text GLabel 5650 2600 0    50   Input ~ 0
B_A3
Text GLabel 5650 2700 0    50   Input ~ 0
B_A1
Text GLabel 5650 4800 0    50   Input ~ 0
B_PSEL*
NoConn ~ 5650 3600
NoConn ~ 5650 3100
NoConn ~ 3350 3700
NoConn ~ 3350 3200
Text GLabel 3350 5200 0    50   Input ~ 0
GND
Text GLabel 3350 2100 0    50   Input ~ 0
+5V
Text GLabel 5650 4900 0    50   Input ~ 0
B_CLKOUT
Text GLabel 3350 3000 0    50   Input ~ 0
B_READY
Text GLabel 3350 4600 0    50   Input ~ 0
B_BST3
Text GLabel 3350 4300 0    50   Input ~ 0
B_BST1
Text GLabel 5650 4200 0    50   Input ~ 0
B_WE*
Text GLabel 5650 4400 0    50   Input ~ 0
B_RD*
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 3 1 5F02AAE1
P 5650 5200
AR Path="/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5F025AB5/5F02AAE1" Ref="J2"  Part="3" 
AR Path="/5ED05929/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5EF2AEDB/5F02AAE1" Ref="J1"  Part="3" 
F 0 "J1" V 4997 3650 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 4906 3650 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 6050 2000 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 4800 2100 50  0001 L CNN
	3    5650 5200
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 1 1 5F02AAE7
P 3350 5200
AR Path="/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5F025AB5/5F02AAE7" Ref="J2"  Part="1" 
AR Path="/5ED05929/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5EF2AEDB/5F02AAE7" Ref="J1"  Part="1" 
F 0 "J1" V 2697 3650 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 2606 3650 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 3750 2000 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2500 2100 50  0001 L CNN
	1    3350 5200
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 2 1 5F02AAED
P 4500 5200
AR Path="/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5F025AB5/5F02AAED" Ref="J2"  Part="2" 
AR Path="/5ED05929/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5EF2AEDB/5F02AAED" Ref="J1"  Part="2" 
F 0 "J1" V 3877 3650 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 3786 3650 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4900 2000 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 3650 2100 50  0001 L CNN
	2    4500 5200
	-1   0    0    1   
$EndComp
Text GLabel 3350 2300 0    50   Input ~ 0
B_D14
Text GLabel 3350 2200 0    50   Input ~ 0
B_D13
Text GLabel 3350 2500 0    50   Input ~ 0
B_D12
Text GLabel 3350 2400 0    50   Input ~ 0
B_D11
Text GLabel 5650 2400 0    50   Input ~ 0
B_D10
Text GLabel 5650 3400 0    50   Input ~ 0
B_D9
Text GLabel 5650 2200 0    50   Input ~ 0
B_D8
Text GLabel 5650 2300 0    50   Input ~ 0
B_D15_IN
Text Notes 600  4500 0    50   ~ 0
In 8-bit  CRU access we\nwant the byte on D0-D7\nnot D8-D15\nDefine D0-D7 as the even word.\nThough D7 is still most significant.
Text GLabel 5650 5000 0    50   UnSpc ~ 0
B_MEM*
Text Notes 600  5150 0    50   ~ 0
Address word is rotated 1 right.\nPSEL* is moved to \nA15 MSBit of address\n(not symmetric with data bus)
Text GLabel 3350 3100 0    50   Input ~ 0
B_HOLD*
Text GLabel 5650 3000 0    50   Input ~ 0
B_FA11
Text GLabel 5650 3200 0    50   Input ~ 0
B_FA12
Text GLabel 5650 3300 0    50   Input ~ 0
B_FA13
Text GLabel 5650 4300 0    50   UnSpc ~ 0
B_FA15
Text GLabel 5650 3900 0    50   Input ~ 0
B_FA16
Text GLabel 3350 3400 0    50   Input ~ 0
B_FA14
Text GLabel 3350 4100 0    50   Input ~ 0
B_FA17
Text GLabel 3350 4200 0    50   Input ~ 0
B_FA18
Text Notes 600  5650 0    50   ~ 0
BMO   Bus Master Override\nPARENA Parallel CRU Enable (except /IORQ)\nSERENA  Serial CRU Enable
Text GLabel 3350 4700 0    50   Input ~ 0
B_IORQ*
Text GLabel 4500 4300 0    50   Input ~ 0
B_BMO
Text GLabel 4500 4500 0    50   Input ~ 0
B_INTA*
Text GLabel 4500 2200 0    50   Input ~ 0
B_FA19
Text GLabel 4500 2300 0    50   Input ~ 0
B_FA20
Text GLabel 4500 2400 0    50   Input ~ 0
B_FA21
Text GLabel 4500 2500 0    50   Input ~ 0
B_FA22
Text GLabel 4500 5000 0    50   Input ~ 0
B_FA23
Text GLabel 4500 5100 0    50   Input ~ 0
B_PRO
Text GLabel 3350 3600 0    50   Input ~ 0
B_PBPTHP
Text GLabel 5650 3500 0    50   Input ~ 0
B_SPAMM
Text GLabel 4500 4100 0    50   Input ~ 0
B_IC0
Text GLabel 4500 4000 0    50   Input ~ 0
B_IC1
Text GLabel 4500 3900 0    50   Input ~ 0
B_IC2
Text GLabel 4500 3800 0    50   Input ~ 0
B_IC3
Text GLabel 4500 3700 0    50   Input ~ 0
B_INT1
Text GLabel 4500 3600 0    50   Input ~ 0
B_INT2
Text GLabel 4500 3500 0    50   Input ~ 0
B_INT3
Text GLabel 4500 3400 0    50   Input ~ 0
B_INT4
Text GLabel 3350 4000 0    50   Input ~ 0
B_IAQ*
Text GLabel 3350 5100 0    50   Input ~ 0
B_HOLDA*
Text GLabel 4500 4800 0    50   Input ~ 0
B_SERENA*
Text GLabel 4500 4900 0    50   Input ~ 0
B_PARENA*
Text Notes 650  3900 0    50   ~ 0
Memory mapper outputs effective address \non A16-A28.   A0 is LSB here.\n\nBank.................... Word Address\nA14 A13 A12 A11 A10..A0\n8 bit Page. ..4K\nA23 .. A16  A15\nExtended Address\n\nA0  \\n...  } Word Address\nA10 /\n\nA11  \\nA12  4} Bank Reg#\nA13   }\nA14  /\nA15  } PSEL* or Map Enable\n\nEffective Address\n\nA16  FA11 4K half-page\n\nA17  FA12 \\n...       8} 8K page#\nA24  FA19 /\n\nA25  \\nA26   } 32MB Expansion\nA27   }\nA28  /\n     \ .. 3 mode bits ..\nA29  } RO      Read-Only\nA30  } PBPTHP  P-Box Pass-Thru Page\nA31  } SPAMM   Set page memory-mapped
NoConn ~ 3350 3300
NoConn ~ 3350 3900
NoConn ~ 3350 4400
NoConn ~ 4500 4700
NoConn ~ 4500 4600
NoConn ~ 4500 4400
NoConn ~ 4500 4200
Text GLabel 5650 4500 0    50   Input ~ 0
B_RDBENA*
Text Notes 600  5950 0    50   ~ 0
HALT is unused right?
Text GLabel 4100 6400 0    50   Input ~ 0
B_FA11
Text GLabel 4100 6500 0    50   Input ~ 0
B_FA12
Text GLabel 4100 6600 0    50   Input ~ 0
B_FA13
Text GLabel 4100 6900 0    50   Input ~ 0
B_FA16
NoConn ~ 4100 6400
NoConn ~ 4100 6500
NoConn ~ 4100 6600
NoConn ~ 4100 6700
NoConn ~ 4100 6800
NoConn ~ 4100 6900
NoConn ~ 4100 7000
NoConn ~ 4100 7100
NoConn ~ 4100 7200
NoConn ~ 4100 7300
NoConn ~ 4100 7400
NoConn ~ 4100 7500
NoConn ~ 4100 7600
NoConn ~ 4550 6400
NoConn ~ 4550 6500
NoConn ~ 4550 6600
NoConn ~ 4550 6900
NoConn ~ 4550 7000
NoConn ~ 4550 7100
NoConn ~ 4550 7200
NoConn ~ 3350 3500
Text GLabel 5650 4600 0    50   Input ~ 0
B_RESOUT*
Text Notes 6000 2250 0    50   ~ 0
LSBit of MSByte (even byte)
Text Notes 6000 2350 0    50   ~ 0
MSBit of MSByte (even byte)
Text Notes 5650 7600 0    50   ~ 0
B_D15 D7    MSBit\nB_D14 D6\nB_D13 D5\nB_D12 D4\nB_D11 D3\nB_D10 D2\nB_D9  D1\nB_D8  D0\nB_D7 D15\nB_D6 D14\nB_D5 D13\nB_D4 D12\nB_D3 D11\nB_D2 D10\nB_D1 D9\nB_D0 D8      LSBit\n
Text Notes 6000 2550 0    50   ~ 0
LSBit, A0 addressing the word
Text GLabel 5650 3700 0    50   Input ~ 0
B_A11
Text GLabel 5650 3800 0    50   Input ~ 0
B_A10
Text Notes 2600 1600 0    50   ~ 0
Kontron Bus used in ECB RetroBrewComputing.net\n\nPins are numbered with 0 least significant.\nData bus 0-7 is from 8-bit bus, so it becomes the even (assuming big-endian), MSB of 16-bit bus.\nA0 is the LSBit of a word address. There is no byte address bit because 16-bit bus.\n99105 IN and OUT pins are in the expected place in the data bus word (not the addr).\n\nMEM* asserts for a CPU memory cycle. CRU requires decoding BST1-3 for IO.\nSERENA* and PARENA* assert for a bit-serial or byte/word-parallel CRU cycle.\nIORQ* asserts for a byte/word-parallel cycle to addresses 00-1FE (MSX style port)\n\nA card can use the data bus as AD0-AD15/PSEL of the 99105,\nor use separate address and data buses.
Text Notes 2600 5900 0    79   ~ 0
All Cards Except CPU will name least significiant \nwith 0! B_A0 is least significant word address (9900\n and 99105 have no A15) B_D0 is least significant data \nbit.\n
Text GLabel 5650 2500 0    50   Input ~ 0
B_A0
NoConn ~ 4550 7300
NoConn ~ 4550 7400
NoConn ~ 4550 7500
NoConn ~ 4550 7600
Text GLabel 5050 6400 0    50   Input ~ 0
B_NMI*
Text GLabel 5050 6500 0    50   Input ~ 0
B_INTREQ*
Text GLabel 5050 6600 0    50   Input ~ 0
B_CLKOUT
Text GLabel 5050 6700 0    50   Input ~ 0
B_RESET*
Text GLabel 5050 6800 0    50   Input ~ 0
B_INTA*
Text GLabel 5050 6900 0    50   Input ~ 0
B_READY
Text GLabel 5050 7000 0    50   Input ~ 0
B_HOLD*
Text GLabel 5050 7100 0    50   Input ~ 0
B_IAQ*
Text GLabel 5050 7300 0    50   Input ~ 0
B_ALATCH
Text GLabel 5050 7200 0    50   Input ~ 0
B_IORQ*
Text GLabel 5050 7400 0    50   Input ~ 0
B_HOLDA*
NoConn ~ 5050 7200
NoConn ~ 5050 7300
NoConn ~ 5050 7400
NoConn ~ 5050 6400
NoConn ~ 5050 6500
NoConn ~ 5050 6600
NoConn ~ 5050 6700
NoConn ~ 5050 6800
NoConn ~ 5050 6900
NoConn ~ 5050 7000
NoConn ~ 5050 7100
Text Notes 600  7700 0    50   ~ 0
MEM* BST1 2 3  Name      Description\n 0      0 0 0  SOPL       \n 0      0 0 1  SOP        \n 0      0 1 0  IOP        \n 0      0 1 1  IAQ*       \n 0      1 0 0  DOP        \n 0      1 0 1  INTA       \n 0      1 1 0  WS         \n 0      1 1 1  GM         General memory transfer\n 1      0 0 0  AUMSL      Internal ALU op or Macrostore access +MPICLK \n 1      0 0 1  AUMS       Internal ALU op or Macrostore access.\n 1      0 1 0  RESET      Reset. The RESET* line is pulled low.\n 1      0 1 1  IO         I/O transfer (a CRU cycle)\n 1      1 0 0  WP         Workspace pointer update\n 1      1 0 1  ST         Status register update\n 1      1 1 0  MID        Macroinstruction detected. APP* sampled on READY\n 1      1 1 1  HOLDA      Hold acknowledge\n
Text Notes 7250 6850 0    100  ~ 0
Backplane signals\nFrom BIOS card schematic
Text Notes 4450 6350 2    50   ~ 0
Unused:
Text GLabel 4550 6600 0    50   Input ~ 0
B_PBPTHP
Text GLabel 4550 6500 0    50   Input ~ 0
B_PRO
Text GLabel 4550 6400 0    50   Input ~ 0
B_SPAMM
Text GLabel 5050 7500 0    50   Input ~ 0
B_PARENA*
NoConn ~ 5050 7500
Text GLabel 5050 7600 0    50   Input ~ 0
B_BST3
NoConn ~ 5050 7600
$Comp
L Gemini:74ALS645A U?
U 1 1 5EFEA50B
P 10400 5550
AR Path="/5EFEA50B" Ref="U?"  Part="1" 
AR Path="/5EF2AEDB/5EFEA50B" Ref="U5"  Part="1" 
F 0 "U5" H 10400 6531 50  0000 C CNN
F 1 "74HCT245NS" H 10400 6440 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 10400 5550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 10400 5550 50  0001 C CNN
	1    10400 5550
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645A U?
U 1 1 5EFEB0F3
P 10400 3500
AR Path="/5EFEB0F3" Ref="U?"  Part="1" 
AR Path="/5EF2AEDB/5EFEB0F3" Ref="U3"  Part="1" 
F 0 "U3" H 10400 4481 50  0000 C CNN
F 1 "74HCT245NS" H 10400 4390 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 10400 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 10400 3500 50  0001 C CNN
	1    10400 3500
	1    0    0    -1  
$EndComp
Text Notes 7100 2800 0    100  ~ 0
Address Bus
Text Notes 9900 2400 0    100  ~ 0
Data Bus
Text Notes 7300 700  0    100  ~ 0
Controls
Text GLabel 10400 4750 0    50   Input ~ 0
+5V
Text GLabel 8150 750  0    50   Input ~ 0
+5V
Text GLabel 10400 2700 0    50   Input ~ 0
+5V
Text GLabel 7650 3100 0    50   Input ~ 0
B_A8
Text GLabel 7650 3200 0    50   Input ~ 0
B_A5
Text GLabel 7650 3300 0    50   Input ~ 0
B_A1
Text GLabel 7650 3400 0    50   Input ~ 0
B_A4
Text GLabel 7650 3500 0    50   Input ~ 0
B_A3
Text GLabel 7650 3600 0    50   Input ~ 0
B_A2
Text GLabel 7650 3700 0    50   Input ~ 0
B_A0
Text GLabel 7650 5650 0    50   Input ~ 0
B_A7
Text GLabel 7650 5550 0    50   Input ~ 0
B_A11
Text GLabel 7650 5350 0    50   Input ~ 0
B_A10
Text GLabel 7650 5450 0    50   Input ~ 0
B_A14
Text GLabel 7650 5250 0    50   Input ~ 0
B_A13
Text GLabel 7650 3000 0    50   Input ~ 0
B_A6
Text GLabel 9900 3700 0    50   Input ~ 0
B_D0_OUT
Text GLabel 9900 3600 0    50   Input ~ 0
B_D1
Text GLabel 9900 3500 0    50   Input ~ 0
B_D2
Text GLabel 9900 3400 0    50   Input ~ 0
B_D3
Text GLabel 9900 3300 0    50   Input ~ 0
B_D4
Text GLabel 9900 3200 0    50   Input ~ 0
B_D5
Text GLabel 9900 3100 0    50   Input ~ 0
B_D6
Text GLabel 9900 3000 0    50   Input ~ 0
B_D7
Text GLabel 9900 5050 0    50   Input ~ 0
B_D8
Text GLabel 9900 5150 0    50   Input ~ 0
B_D9
Text GLabel 9900 5250 0    50   Input ~ 0
B_D10
Text GLabel 9900 5350 0    50   Input ~ 0
B_D11
Text GLabel 9900 5450 0    50   Input ~ 0
B_D12
Text GLabel 9900 5550 0    50   Input ~ 0
B_D13
Text GLabel 9900 5650 0    50   Input ~ 0
B_D14
Text GLabel 9900 5750 0    50   Input ~ 0
B_D15_IN
Text GLabel 7650 5050 0    50   Input ~ 0
B_A12
Text GLabel 8650 3000 2    50   Input ~ 0
A6
Text GLabel 8650 3100 2    50   Input ~ 0
A8
Text GLabel 8650 3200 2    50   Input ~ 0
A5
Text GLabel 8650 3300 2    50   Input ~ 0
A1
Text GLabel 8650 3400 2    50   Input ~ 0
A4
Text GLabel 8650 3500 2    50   Input ~ 0
A3
Text GLabel 8650 3600 2    50   Input ~ 0
A2
Text GLabel 8650 3700 2    50   Input ~ 0
A0
Text GLabel 7650 1450 0    50   Input ~ 0
B_RD*
Text GLabel 7650 1350 0    50   Input ~ 0
B_RDBENA*
Text GLabel 7650 1150 0    50   Input ~ 0
B_PSEL*
Text GLabel 7650 1250 0    50   Input ~ 0
B_SERENA*
Text GLabel 7650 1550 0    50   Input ~ 0
B_BST2
Text GLabel 7650 1650 0    50   Input ~ 0
B_BST1
Text GLabel 7650 1050 0    50   UnSpc ~ 0
B_MEM*
Text GLabel 7650 1750 0    50   Input ~ 0
B_WE*
Text GLabel 8650 1450 2    50   Input ~ 0
RD*
Text GLabel 8650 1350 2    50   Input ~ 0
RDBENA*
Text GLabel 8650 1150 2    50   Input ~ 0
PSEL*
Text GLabel 8650 1250 2    50   Input ~ 0
SERENA*
Text GLabel 8650 1550 2    50   Input ~ 0
BST2
Text GLabel 8650 1650 2    50   Input ~ 0
BST1
Text GLabel 8650 1050 2    50   UnSpc ~ 0
MEM*
Text GLabel 8650 1750 2    50   Input ~ 0
WE*
Text GLabel 10400 6350 0    50   Input ~ 0
GND
Text GLabel 10400 4300 0    50   Input ~ 0
GND
Text GLabel 10900 5050 2    50   Input ~ 0
D8
Text GLabel 10900 5150 2    50   Input ~ 0
D9
Text GLabel 10900 5250 2    50   Input ~ 0
D10
Text GLabel 10900 5350 2    50   Input ~ 0
D11
Text GLabel 10900 5450 2    50   Input ~ 0
D12
Text GLabel 10900 5550 2    50   Input ~ 0
D13
Text GLabel 10900 5650 2    50   Input ~ 0
D14
Text GLabel 10900 5750 2    50   Input ~ 0
D15
Text GLabel 10900 3700 2    50   Input ~ 0
D0
Text GLabel 10900 3600 2    50   Input ~ 0
D1
Text GLabel 10900 3500 2    50   Input ~ 0
D2
Text GLabel 10900 3400 2    50   Input ~ 0
D3
Text GLabel 10900 3300 2    50   Input ~ 0
D4
Text GLabel 10900 3200 2    50   Input ~ 0
D5
Text GLabel 10900 3100 2    50   Input ~ 0
D6
Text GLabel 10900 3000 2    50   Input ~ 0
D7
Text GLabel 9900 5950 0    50   Input ~ 0
RDBENA*
Text GLabel 9900 3900 0    50   Input ~ 0
RDBENA*
Text GLabel 9900 6050 0    50   Input ~ 0
MEMSEL*
Text GLabel 9900 4000 0    50   Input ~ 0
MEMSEL*
Text GLabel 10050 1250 2    50   Input ~ 0
CRUIN
Text GLabel 9450 1250 0    50   Input ~ 0
B_D15_IN
Text GLabel 8650 5650 2    50   Input ~ 0
A7
Text GLabel 8650 5550 2    50   Input ~ 0
A11
Text GLabel 8650 5350 2    50   Input ~ 0
A10
Text GLabel 8650 5450 2    50   Input ~ 0
A14
Text GLabel 8650 5250 2    50   Input ~ 0
A13
Text GLabel 8650 5150 2    50   Input ~ 0
A9
Text GLabel 8650 5050 2    50   Input ~ 0
A12
Text GLabel 4550 6800 0    50   Input ~ 0
B_BMO
Text GLabel 4550 7600 0    50   Input ~ 0
B_IC0
Text GLabel 4550 7500 0    50   Input ~ 0
B_IC1
Text GLabel 4550 7400 0    50   Input ~ 0
B_IC2
Text GLabel 4550 7300 0    50   Input ~ 0
B_IC3
Text GLabel 4550 7200 0    50   Input ~ 0
B_INT1
Text GLabel 4550 7100 0    50   Input ~ 0
B_INT2
Text GLabel 4550 7000 0    50   Input ~ 0
B_INT3
Text GLabel 4550 6900 0    50   Input ~ 0
B_INT4
Text GLabel 4100 7200 0    50   Input ~ 0
B_FA19
Text GLabel 4100 7300 0    50   Input ~ 0
B_FA20
Text GLabel 4100 7400 0    50   Input ~ 0
B_FA21
Text GLabel 4100 7500 0    50   Input ~ 0
B_FA22
Text GLabel 4100 7600 0    50   Input ~ 0
B_FA23
Text GLabel 4100 6800 0    50   UnSpc ~ 0
B_FA15
Text GLabel 4100 7000 0    50   Input ~ 0
B_FA17
Text GLabel 4100 7100 0    50   Input ~ 0
B_FA18
Text GLabel 4100 6700 0    50   Input ~ 0
B_FA14
NoConn ~ 4550 6800
NoConn ~ 10400 700 
NoConn ~ 9450 700 
Text GLabel 10950 1150 0    50   Input ~ 0
+5V
Text GLabel 10700 450  0    50   Input ~ 0
+5V
Text GLabel 7650 5150 0    50   Input ~ 0
B_A9
Text GLabel 5550 6700 0    50   Input ~ 0
B_RESOUT*
NoConn ~ 5550 6700
$Comp
L 74xx:74LS125 U6
U 1 1 5F08D578
P 9750 1900
F 0 "U6" H 9750 2217 50  0000 C CNN
F 1 "74LS125" H 9750 2126 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9750 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9750 1900 50  0001 C CNN
	1    9750 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U6
U 5 1 5F12A909
P 10950 1650
F 0 "U6" H 10720 1604 50  0000 R CNN
F 1 "74LS125" H 10720 1695 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10950 1650 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10950 1650 50  0001 C CNN
	5    10950 1650
	-1   0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U6
U 3 1 5F09271D
P 9750 1250
F 0 "U6" H 9750 1567 50  0000 C CNN
F 1 "74LS125" H 9750 1476 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9750 1250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9750 1250 50  0001 C CNN
	3    9750 1250
	-1   0    0    -1  
$EndComp
$Comp
L 74xx:74LS125 U6
U 4 1 5F12A0B2
P 10700 700
F 0 "U6" H 10700 925 50  0000 C CNN
F 1 "74LS125" H 10700 1016 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10700 700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10700 700 50  0001 C CNN
	4    10700 700 
	-1   0    0    1   
$EndComp
Wire Notes Line
	10450 1650 10450 2200
Wire Notes Line
	10450 2200 9050 2200
Wire Notes Line
	9050 2200 9050 1650
Wire Notes Line
	9050 1650 10450 1650
Text Notes 9050 1850 0    50   ~ 0
If U3 is omitted, then B_D_OUT to CRUOUT\nand GND to E*
Text GLabel 9450 1900 0    50   Input ~ 0
+5V
Text GLabel 9750 2150 0    50   Input ~ 0
+5V
NoConn ~ 10050 1900
Text GLabel 9750 1500 2    50   Input ~ 0
BANKSEL*
$Comp
L 74xx:74LS125 U6
U 2 1 5F12A3C6
P 9750 700
F 0 "U6" H 9750 925 50  0000 C CNN
F 1 "74LS125" H 9750 1016 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9750 700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9750 700 50  0001 C CNN
	2    9750 700 
	-1   0    0    1   
$EndComp
Text GLabel 10950 2150 0    50   Input ~ 0
GND
Text GLabel 10050 700  2    50   Input ~ 0
GND
Text GLabel 9750 450  0    50   Input ~ 0
GND
Text GLabel 11000 700  2    50   Input ~ 0
GND
Wire Wire Line
	7650 1050 8650 1050
Wire Wire Line
	8650 1150 7650 1150
Wire Wire Line
	7650 1250 8650 1250
Wire Wire Line
	8650 1350 7650 1350
Wire Wire Line
	7650 1450 8650 1450
Wire Wire Line
	8650 1550 7650 1550
Wire Wire Line
	7650 1650 8650 1650
Wire Wire Line
	8650 1750 7650 1750
Wire Wire Line
	7650 3000 8650 3000
Wire Wire Line
	8650 3100 7650 3100
Wire Wire Line
	7650 3200 8650 3200
Wire Wire Line
	8650 3300 7650 3300
Wire Wire Line
	7650 3400 8650 3400
Wire Wire Line
	8650 3500 7650 3500
Wire Wire Line
	7650 3600 8650 3600
Wire Wire Line
	8650 3700 7650 3700
Wire Wire Line
	7650 5050 8650 5050
Wire Wire Line
	8650 5150 7650 5150
Wire Wire Line
	7650 5250 8650 5250
Wire Wire Line
	8650 5350 7650 5350
Wire Wire Line
	7650 5450 8650 5450
Wire Wire Line
	8650 5550 7650 5550
Wire Wire Line
	7650 5650 8650 5650
Text Notes 9000 650  0    50   ~ 0
Spare '125 could\nbe inverters
Text Notes 11250 5750 0    50   ~ 0
Optimal for Bus routing:\nD9\nD12\nD10\nD11\nD15\nD14\nD8\nD13\n
$EndSCHEMATC
