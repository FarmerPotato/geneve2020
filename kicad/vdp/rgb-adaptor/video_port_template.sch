EESchema Schematic File Version 4
LIBS:vga-adaptor-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J?
U 1 1 604DA6B2
P 2500 3900
F 0 "J?" H 2580 3892 50  0000 L CNN
F 1 "Conn" H 2580 3801 50  0001 L CNN
F 2 "Connector_IDC:IDC-Header_2x08_P2.54mm_Horizontal" H 2500 3900 50  0001 C CNN
F 3 "~" H 2500 3900 50  0001 C CNN
	1    2500 3900
	-1   0    0    1   
$EndComp
Text GLabel 2200 3900 0    50   Input ~ 0
RED_OUT
Text GLabel 2200 3800 0    50   Input ~ 0
GREEN_OUT
Text GLabel 2200 3700 0    50   Input ~ 0
BLUE_OUT
Text GLabel 2700 4100 2    50   Input ~ 0
GND
Text GLabel 2200 4100 0    50   Input ~ 0
VSYNC_OUT
Text GLabel 2700 3600 2    50   Input ~ 0
CVIDEO
Text GLabel 2200 4000 0    50   Input ~ 0
HSYNC_OUT
Text Notes 1950 3300 0    50   ~ 0
Cable To Video Port
Text GLabel 2700 4200 2    50   Input ~ 0
GND
Text GLabel 2700 3700 2    50   Input ~ 0
GND
Text GLabel 2700 3800 2    50   Input ~ 0
GND
Text GLabel 2700 3900 2    50   Input ~ 0
GND
Text GLabel 2200 3500 0    50   Input ~ 0
+5V
Text GLabel 2700 3500 2    50   Input ~ 0
SVIDEO-C
Text GLabel 2200 3600 0    50   Input ~ 0
SVIDEO-Y
Text GLabel 2700 4000 2    50   Input ~ 0
GND
Text GLabel 2200 4200 0    50   Input ~ 0
CSYNC_OUT
$EndSCHEMATC
