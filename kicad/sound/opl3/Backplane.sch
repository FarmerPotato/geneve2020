EESchema Schematic File Version 4
LIBS:opl3-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4500 2100 0    50   Input ~ 0
+5V
Text GLabel 5650 4100 0    50   Input ~ 0
B_INTREQ*
Text GLabel 4500 5200 0    50   Input ~ 0
GND
Text GLabel 5650 4000 0    50   Input ~ 0
B_NMI*
Text GLabel 4500 2800 0    50   Input ~ 0
B_D2
Text GLabel 4500 3000 0    50   Input ~ 0
B_D4
Text GLabel 4500 3200 0    50   Input ~ 0
B_D6
Text GLabel 4500 2600 0    50   Input ~ 0
B_D0_OUT
Text GLabel 3350 4800 0    50   Input ~ 0
B_ALATCH
Text GLabel 5650 5200 0    50   Input ~ 0
GND
Text GLabel 5650 2100 0    50   Input ~ 0
+5V
Text GLabel 5650 5100 0    50   Input ~ 0
B_RESET*
Text GLabel 3350 4500 0    50   Input ~ 0
B_BST2
Text GLabel 4500 3300 0    50   Input ~ 0
B_D7
Text GLabel 4500 3100 0    50   Input ~ 0
B_D5
Text GLabel 4500 2900 0    50   Input ~ 0
B_D3
Text GLabel 4500 2700 0    50   Input ~ 0
B_D1
Text GLabel 3350 2900 0    50   Input ~ 0
B_A6
Text GLabel 5650 2800 0    50   Input ~ 0
B_A8
Text GLabel 3350 3800 0    50   Input ~ 0
B_A14
Text GLabel 5650 4700 0    50   Input ~ 0
B_A12
Text GLabel 3350 2700 0    50   Input ~ 0
B_A4
Text GLabel 3350 2600 0    50   Input ~ 0
B_A2
Text GLabel 3350 4900 0    50   Input ~ 0
B_A13
Text GLabel 5650 2900 0    50   Input ~ 0
B_A7
Text GLabel 3350 5000 0    50   Input ~ 0
B_A9
Text GLabel 3350 2800 0    50   Input ~ 0
B_A5
Text GLabel 5650 2600 0    50   Input ~ 0
B_A3
Text GLabel 5650 2700 0    50   Input ~ 0
B_A1
Text GLabel 5650 4800 0    50   Input ~ 0
B_PSEL*
NoConn ~ 5650 3600
NoConn ~ 5650 3100
NoConn ~ 3350 3700
NoConn ~ 3350 3200
Text GLabel 3350 5200 0    50   Input ~ 0
GND
Text GLabel 3350 2100 0    50   Input ~ 0
+5V
Text GLabel 5650 4900 0    50   Input ~ 0
B_CLKOUT
Text GLabel 3350 3000 0    50   Input ~ 0
B_READY
Text GLabel 3350 4600 0    50   Input ~ 0
B_BST3
Text GLabel 3350 4300 0    50   Input ~ 0
B_BST1
Text GLabel 5650 4200 0    50   Input ~ 0
B_WE*
Text GLabel 5650 4400 0    50   Input ~ 0
B_RD*
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 3 1 5F02AAE1
P 5650 5200
AR Path="/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5F025AB5/5F02AAE1" Ref="J2"  Part="3" 
AR Path="/5ED05929/5F02AAE1" Ref="J?"  Part="3" 
AR Path="/5EF2AEDB/5F02AAE1" Ref="J1"  Part="3" 
AR Path="/5F4BC2EA/5F02AAE1" Ref="J1"  Part="3" 
AR Path="/5F2FEF9D/5F02AAE1" Ref="J1"  Part="3" 
F 0 "J1" V 4997 3650 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 4906 3650 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 6050 2000 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 4800 2100 50  0001 L CNN
	3    5650 5200
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 1 1 5F02AAE7
P 3350 5200
AR Path="/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5F025AB5/5F02AAE7" Ref="J2"  Part="1" 
AR Path="/5ED05929/5F02AAE7" Ref="J?"  Part="1" 
AR Path="/5EF2AEDB/5F02AAE7" Ref="J1"  Part="1" 
AR Path="/5F4BC2EA/5F02AAE7" Ref="J1"  Part="1" 
AR Path="/5F2FEF9D/5F02AAE7" Ref="J1"  Part="1" 
F 0 "J1" V 2697 3650 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 2606 3650 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 3750 2000 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 2500 2100 50  0001 L CNN
	1    3350 5200
	-1   0    0    1   
$EndComp
$Comp
L Gemini:DIN-41612_Kontron_Card J?
U 2 1 5F02AAED
P 4500 5200
AR Path="/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5F025AB5/5F02AAED" Ref="J2"  Part="2" 
AR Path="/5ED05929/5F02AAED" Ref="J?"  Part="2" 
AR Path="/5EF2AEDB/5F02AAED" Ref="J1"  Part="2" 
AR Path="/5F4BC2EA/5F02AAED" Ref="J1"  Part="2" 
AR Path="/5F2FEF9D/5F02AAED" Ref="J1"  Part="2" 
F 0 "J1" V 3877 3650 50  0000 C CNN
F 1 "DIN-41612_Kontron_Card" V 3786 3650 50  0000 C CNN
F 2 "Connector_DIN:DIN41612_C_3x32_Male_Horizontal_THT" H 4900 2000 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/86092645113755elf/amphenol-fci" H 3650 2100 50  0001 L CNN
	2    4500 5200
	-1   0    0    1   
$EndComp
Text GLabel 3350 2300 0    50   Input ~ 0
B_D14
Text GLabel 3350 2200 0    50   Input ~ 0
B_D13
Text GLabel 3350 2500 0    50   Input ~ 0
B_D12
Text GLabel 3350 2400 0    50   Input ~ 0
B_D11
Text GLabel 5650 2400 0    50   Input ~ 0
B_D10
Text GLabel 5650 3400 0    50   Input ~ 0
B_D9
Text GLabel 5650 2200 0    50   Input ~ 0
B_D8
Text GLabel 5650 2300 0    50   Input ~ 0
B_D15_IN
Text Notes 600  4500 0    50   ~ 0
In 8-bit  CRU access we\nwant the byte on D0-D7\nnot D8-D15\nDefine D0-D7 as the even word.\nThough D7 is still most significant.
Text GLabel 5650 5000 0    50   UnSpc ~ 0
B_MEM*
Text Notes 600  5150 0    50   ~ 0
Address word is rotated 1 right.\nPSEL* is moved to \nA15 MSBit of address\n(not symmetric with data bus)
Text GLabel 3350 3100 0    50   Input ~ 0
B_HOLD*
Text GLabel 5650 3000 0    50   Input ~ 0
B_FA11
Text GLabel 5650 3200 0    50   Input ~ 0
B_FA12
Text GLabel 5650 3300 0    50   Input ~ 0
B_FA13
Text GLabel 5650 4300 0    50   UnSpc ~ 0
B_FA15
Text GLabel 5650 3900 0    50   Input ~ 0
B_FA16
Text GLabel 3350 3400 0    50   Input ~ 0
B_FA14
Text GLabel 3350 4100 0    50   Input ~ 0
B_FA17
Text GLabel 3350 4200 0    50   Input ~ 0
B_FA18
Text Notes 600  5650 0    50   ~ 0
BMO   Bus Master Override\nPARENA Parallel CRU Enable (except /IORQ)\nSERENA  Serial CRU Enable
Text GLabel 3350 4700 0    50   Input ~ 0
B_IORQ*
Text GLabel 4500 4300 0    50   Input ~ 0
B_BMO
Text GLabel 4500 4500 0    50   Input ~ 0
B_INTA*
Text GLabel 4500 2200 0    50   Input ~ 0
B_FA19
Text GLabel 4500 2300 0    50   Input ~ 0
B_FA20
Text GLabel 4500 2400 0    50   Input ~ 0
B_FA21
Text GLabel 4500 2500 0    50   Input ~ 0
B_FA22
Text GLabel 4500 5000 0    50   Input ~ 0
B_FA23
Text GLabel 4500 5100 0    50   Input ~ 0
B_PRO
Text GLabel 3350 3600 0    50   Input ~ 0
B_PBPTHP
Text GLabel 5650 3500 0    50   Input ~ 0
B_SPAMM
Text GLabel 4500 4100 0    50   Input ~ 0
B_IC0
Text GLabel 4500 4000 0    50   Input ~ 0
B_IC1
Text GLabel 4500 3900 0    50   Input ~ 0
B_IC2
Text GLabel 4500 3800 0    50   Input ~ 0
B_IC3
Text GLabel 4500 3700 0    50   Input ~ 0
B_INT1*
Text GLabel 4500 3600 0    50   Input ~ 0
B_INT4*
Text GLabel 4500 3500 0    50   Input ~ 0
B_INT6*
Text GLabel 4500 3400 0    50   Input ~ 0
B_INT9*
Text GLabel 3350 4000 0    50   Input ~ 0
B_IAQ*
Text GLabel 3350 5100 0    50   Input ~ 0
B_HOLDA*
Text GLabel 4500 4800 0    50   Input ~ 0
B_SERENA*
Text GLabel 4500 4900 0    50   Input ~ 0
B_PARENA*
Text Notes 650  3900 0    50   ~ 0
Memory mapper outputs effective address \non A16-A28.   A0 is LSB here.\n\nBank.................... Word Address\nA14 A13 A12 A11 A10..A0\n8 bit Page. ..4K\nA23 .. A16  A15\nExtended Address\n\nA0  \\n...  } Word Address\nA10 /\n\nA11  \\nA12  4} Bank Reg#\nA13   }\nA14  /\nA15  } PSEL* or Map Enable\n\nEffective Address\n\nA16  FA11 4K half-page\n\nA17  FA12 \\n...       8} 8K page#\nA24  FA19 /\n\nA25  \\nA26   } 32MB Expansion\nA27   }\nA28  /\n     \ .. 3 mode bits ..\nA29  } RO      Read-Only\nA30  } PBPTHP  P-Box Pass-Thru Page\nA31  } SPAMM   Set page memory-mapped
NoConn ~ 3350 3300
NoConn ~ 3350 3900
NoConn ~ 3350 4400
NoConn ~ 4500 4700
NoConn ~ 4500 4600
NoConn ~ 4500 4400
NoConn ~ 4500 4200
Text GLabel 5650 4500 0    50   Input ~ 0
B_RDBENA*
Text Notes 600  5950 0    50   ~ 0
HALT is unused right?
Text GLabel 4100 6400 0    50   Input ~ 0
B_FA11
Text GLabel 4100 6500 0    50   Input ~ 0
B_FA12
Text GLabel 4100 6600 0    50   Input ~ 0
B_FA13
Text GLabel 4100 6900 0    50   Input ~ 0
B_FA16
NoConn ~ 4100 6400
NoConn ~ 4100 6500
NoConn ~ 4100 6600
NoConn ~ 4100 6700
NoConn ~ 4100 6800
NoConn ~ 4100 6900
NoConn ~ 4100 7000
NoConn ~ 4100 7100
NoConn ~ 4100 7200
NoConn ~ 4100 7300
NoConn ~ 4100 7400
NoConn ~ 4100 7500
NoConn ~ 4100 7600
NoConn ~ 4550 6400
NoConn ~ 4550 6500
NoConn ~ 4550 6600
NoConn ~ 3350 3500
Text GLabel 5650 4600 0    50   Input ~ 0
B_RESOUT*
Text Notes 6000 2250 0    50   ~ 0
LSBit of MSByte (even byte)
Text Notes 6000 2350 0    50   ~ 0
MSBit of MSByte (even byte)
Text Notes 6000 7600 0    50   ~ 0
B_D15 D7    MSBit\nB_D14 D6\nB_D13 D5\nB_D12 D4\nB_D11 D3\nB_D10 D2\nB_D9  D1\nB_D8  D0\nB_D7 D15\nB_D6 D14\nB_D5 D13\nB_D4 D12\nB_D3 D11\nB_D2 D10\nB_D1 D9\nB_D0 D8      LSBit\n
Text Notes 6000 2550 0    50   ~ 0
LSBit, A0 addressing the word
Text GLabel 5650 3700 0    50   Input ~ 0
B_A11
Text GLabel 5650 3800 0    50   Input ~ 0
B_A10
Text Notes 2600 1600 0    50   ~ 0
Kontron Bus used in ECB RetroBrewComputing.net\n\nPins are numbered with 0 least significant.\nData bus 0-7 is from 8-bit bus, so it becomes the even (assuming big-endian), MSB of 16-bit bus.\nA0 is the LSBit of a word address. There is no byte address bit because 16-bit bus.\n99105 IN and OUT pins are in the expected place in the data bus word (not the addr).\n\nMEM* asserts for a CPU memory cycle. CRU requires decoding BST1-3 for IO.\nSERENA* and PARENA* assert for a bit-serial or byte/word-parallel CRU cycle.\nIORQ* asserts for a byte/word-parallel cycle to addresses 00-1FE (MSX style port)\n\nA card can use the data bus as AD0-AD15/PSEL of the 99105,\nor use separate address and data buses.
Text Notes 2600 5900 0    79   ~ 0
All Cards Except CPU will name least significiant \nwith 0! B_A0 is least significant word address (9900\n and 99105 have no A15) B_D0 is least significant data \nbit.\n
Text GLabel 5650 2500 0    50   Input ~ 0
B_A0
Text GLabel 5050 6400 0    50   Input ~ 0
B_NMI*
Text GLabel 5050 6700 0    50   Input ~ 0
B_RESET*
Text GLabel 5050 6800 0    50   Input ~ 0
B_INTA*
Text GLabel 5050 6900 0    50   Input ~ 0
B_READY
Text GLabel 5050 7000 0    50   Input ~ 0
B_HOLD*
Text GLabel 5050 7100 0    50   Input ~ 0
B_IAQ*
Text GLabel 5050 7300 0    50   Input ~ 0
B_ALATCH
Text GLabel 5050 7200 0    50   Input ~ 0
B_IORQ*
Text GLabel 5050 7400 0    50   Input ~ 0
B_HOLDA*
NoConn ~ 5050 7200
NoConn ~ 5050 7300
NoConn ~ 5050 7400
NoConn ~ 5050 6400
NoConn ~ 5050 6700
NoConn ~ 5050 6800
NoConn ~ 5050 6900
NoConn ~ 5050 7000
NoConn ~ 5050 7100
Text Notes 600  7700 0    50   ~ 0
MEM* BST1 2 3  Name      Description\n 0      0 0 0  SOPL       \n 0      0 0 1  SOP        \n 0      0 1 0  IOP        \n 0      0 1 1  IAQ*       \n 0      1 0 0  DOP        \n 0      1 0 1  INTA       \n 0      1 1 0  WS         \n 0      1 1 1  GM         General memory transfer\n 1      0 0 0  AUMSL      Internal ALU op or Macrostore access +MPICLK \n 1      0 0 1  AUMS       Internal ALU op or Macrostore access.\n 1      0 1 0  RESET      Reset. The RESET* line is pulled low.\n 1      0 1 1  IO         I/O transfer (a CRU cycle)\n 1      1 0 0  WP         Workspace pointer update\n 1      1 0 1  ST         Status register update\n 1      1 1 0  MID        Macroinstruction detected. APP* sampled on READY\n 1      1 1 1  HOLDA      Hold acknowledge\n
Text Notes 7200 7050 0    100  ~ 0
Backplane signals\nCRU Card\nNo data bus
Text Notes 4450 6350 2    50   ~ 0
Unused:
Text GLabel 4550 6600 0    50   Input ~ 0
B_PBPTHP
Text GLabel 4550 6500 0    50   Input ~ 0
B_PRO
Text GLabel 4550 6400 0    50   Input ~ 0
B_SPAMM
Text GLabel 5050 7600 0    50   Input ~ 0
B_BST2
NoConn ~ 5050 7600
$Comp
L Gemini:74ALS645A U?
U 1 1 5EFE8A23
P 8150 5500
AR Path="/5EFE8A23" Ref="U?"  Part="1" 
AR Path="/5EF2AEDB/5EFE8A23" Ref="U4"  Part="1" 
AR Path="/5F4BC2EA/5EFE8A23" Ref="U4"  Part="1" 
AR Path="/5F2FEF9D/5EFE8A23" Ref="U4"  Part="1" 
F 0 "U4" H 8150 6481 50  0000 C CNN
F 1 "74ALS645A" H 8150 6390 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 8150 5500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 8150 5500 50  0001 C CNN
	1    8150 5500
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645A U?
U 1 1 5EFE8A29
P 8150 1550
AR Path="/5EFE8A29" Ref="U?"  Part="1" 
AR Path="/5EF2AEDB/5EFE8A29" Ref="U1"  Part="1" 
AR Path="/5F4BC2EA/5EFE8A29" Ref="U2"  Part="1" 
AR Path="/5F2FEF9D/5EFE8A29" Ref="U2"  Part="1" 
F 0 "U2" H 8150 2531 50  0000 C CNN
F 1 "74ALS645A" H 8150 2440 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 8150 1550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 8150 1550 50  0001 C CNN
	1    8150 1550
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74ALS645A U?
U 1 1 5EFE8A2F
P 8150 3550
AR Path="/5EFE8A2F" Ref="U?"  Part="1" 
AR Path="/5EF2AEDB/5EFE8A2F" Ref="U2"  Part="1" 
AR Path="/5F4BC2EA/5EFE8A2F" Ref="U3"  Part="1" 
AR Path="/5F2FEF9D/5EFE8A2F" Ref="U3"  Part="1" 
F 0 "U3" H 8150 4531 50  0000 C CNN
F 1 "74ALS645A" H 8150 4440 50  0000 C CNN
F 2 "Gemini:SO-20_5.3x12.6mm_P1.27mm" H 8150 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS245" H 8150 3550 50  0001 C CNN
	1    8150 3550
	1    0    0    -1  
$EndComp
Text Notes 7100 4800 0    100  ~ 0
Address Bus
Text Notes 7300 700  0    100  ~ 0
Controls
Text GLabel 8150 2750 0    50   Input ~ 0
+5V
Text GLabel 8150 4700 0    50   Input ~ 0
+5V
Text GLabel 8150 750  0    50   Input ~ 0
+5V
Text GLabel 7650 5100 0    50   Input ~ 0
B_A8
Text GLabel 7650 5000 0    50   Input ~ 0
B_A5
Text GLabel 7650 5500 0    50   Input ~ 0
B_A1
Text GLabel 7650 5200 0    50   Input ~ 0
B_A4
Text GLabel 7650 5300 0    50   Input ~ 0
B_A3
Text GLabel 7650 5400 0    50   Input ~ 0
B_A2
Text GLabel 7650 5700 0    50   Input ~ 0
B_A0
Text GLabel 7650 3650 0    50   Input ~ 0
B_A7
Text GLabel 7650 3150 0    50   Input ~ 0
B_A11
Text GLabel 7650 3050 0    50   Input ~ 0
B_A10
Text GLabel 7650 3450 0    50   Input ~ 0
B_A13
Text GLabel 7650 3550 0    50   Input ~ 0
B_A6
Text GLabel 7650 3250 0    50   Input ~ 0
B_A12
Text GLabel 8650 3550 2    50   Input ~ 0
A6
Text GLabel 8650 5100 2    50   Input ~ 0
A8
Text GLabel 8650 5000 2    50   Input ~ 0
A5
Text GLabel 8650 5500 2    50   Input ~ 0
A1
Text GLabel 8650 5200 2    50   Input ~ 0
A4
Text GLabel 8650 5300 2    50   Input ~ 0
A3
Text GLabel 8650 5400 2    50   Input ~ 0
A2
Text GLabel 8650 5700 2    50   Input ~ 0
A0
Text GLabel 7650 1350 0    50   Input ~ 0
B_RDBENA*
Text GLabel 7650 1050 0    50   Input ~ 0
B_SERENA*
Text GLabel 7650 1750 0    50   Input ~ 0
B_WE*
Text GLabel 7650 1950 0    50   Input ~ 0
+5V
Text GLabel 7650 2050 0    50   Input ~ 0
GND
Text GLabel 7650 5900 0    50   Input ~ 0
+5V
Text GLabel 7650 3950 0    50   Input ~ 0
+5V
Text GLabel 8650 1350 2    50   Input ~ 0
RDBENA*
Text GLabel 8650 1050 2    50   Input ~ 0
SERENA*
Text GLabel 8150 2350 0    50   Input ~ 0
GND
Text GLabel 8150 4350 0    50   Input ~ 0
GND
Text GLabel 8150 6300 0    50   Input ~ 0
GND
$Comp
L 74xx:74LS125 U5
U 2 1 5F08D578
P 9750 1900
F 0 "U5" H 9750 2217 50  0000 C CNN
F 1 "74LS125" H 9750 2126 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9750 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9750 1900 50  0001 C CNN
	2    9750 1900
	1    0    0    -1  
$EndComp
Text GLabel 8650 1550 2    50   Input ~ 0
CRUOUT01
$Comp
L 74xx:74LS125 U5
U 1 1 5F09271D
P 9750 1250
F 0 "U5" H 9750 1567 50  0000 C CNN
F 1 "74LS125" H 9750 1476 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9750 1250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9750 1250 50  0001 C CNN
	1    9750 1250
	-1   0    0    -1  
$EndComp
Text GLabel 10050 1250 2    50   Input ~ 0
CRUIN
Text GLabel 9450 1250 0    50   Input ~ 0
B_D15_IN
Text GLabel 9750 1500 2    50   Input ~ 0
CRUEN*
Text GLabel 7650 6000 0    50   Input ~ 0
GND
Text GLabel 7650 4050 0    50   Input ~ 0
GND
Text GLabel 8650 3650 2    50   Input ~ 0
A7
Text GLabel 8650 3150 2    50   Input ~ 0
A11
Text GLabel 8650 3050 2    50   Input ~ 0
A10
Text GLabel 8650 3450 2    50   Input ~ 0
A13
Text GLabel 8650 3350 2    50   Input ~ 0
A9
Text GLabel 8650 3250 2    50   Input ~ 0
A12
Text GLabel 4550 6800 0    50   Input ~ 0
B_BMO
Text GLabel 4100 7200 0    50   Input ~ 0
B_FA19
Text GLabel 4100 7300 0    50   Input ~ 0
B_FA20
Text GLabel 4100 7400 0    50   Input ~ 0
B_FA21
Text GLabel 4100 7500 0    50   Input ~ 0
B_FA22
Text GLabel 4100 7600 0    50   Input ~ 0
B_FA23
Text GLabel 4100 6800 0    50   UnSpc ~ 0
B_FA15
Text GLabel 4100 7000 0    50   Input ~ 0
B_FA17
Text GLabel 4100 7100 0    50   Input ~ 0
B_FA18
Text GLabel 4100 6700 0    50   Input ~ 0
B_FA14
NoConn ~ 4550 6800
$Comp
L 74xx:74LS125 U5
U 3 1 5F12A0B2
P 10700 850
F 0 "U5" H 10700 1075 50  0000 C CNN
F 1 "74LS125" H 10700 1166 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10700 850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10700 850 50  0001 C CNN
	3    10700 850 
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LS125 U5
U 5 1 5F12A909
P 10750 2700
F 0 "U5" H 10520 2654 50  0000 R CNN
F 1 "74LS125" H 10520 2745 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10750 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10750 2700 50  0001 C CNN
	5    10750 2700
	1    0    0    -1  
$EndComp
NoConn ~ 10400 850 
NoConn ~ 9450 450 
Text GLabel 10750 2200 0    50   Input ~ 0
+5V
Text GLabel 10750 3200 0    50   Input ~ 0
GND
Text GLabel 10700 600  0    50   Input ~ 0
+5V
Text GLabel 11000 850  2    50   Input ~ 0
+5V
Text GLabel 7650 3350 0    50   Input ~ 0
B_A9
Text GLabel 5050 7500 0    50   Input ~ 0
B_BST1
Text GLabel 5050 7700 0    50   Input ~ 0
B_BST3
NoConn ~ 5050 7500
NoConn ~ 5050 7700
Text GLabel 5600 7500 0    50   Input ~ 0
B_D2
Text GLabel 5600 7300 0    50   Input ~ 0
B_D4
Text GLabel 5600 7100 0    50   Input ~ 0
B_D6
Text GLabel 5600 7000 0    50   Input ~ 0
B_D7
Text GLabel 5600 7200 0    50   Input ~ 0
B_D5
Text GLabel 5600 7400 0    50   Input ~ 0
B_D3
Text GLabel 5600 7600 0    50   Input ~ 0
B_D1
Text GLabel 5600 6300 0    50   Input ~ 0
B_D14
Text GLabel 5600 6400 0    50   Input ~ 0
B_D13
Text GLabel 5600 6500 0    50   Input ~ 0
B_D12
Text GLabel 5600 6600 0    50   Input ~ 0
B_D11
Text GLabel 5600 6700 0    50   Input ~ 0
B_D10
Text GLabel 5600 6900 0    50   Input ~ 0
B_D8
Text GLabel 5600 6800 0    50   Input ~ 0
B_D9
NoConn ~ 5600 6300
NoConn ~ 5600 6400
NoConn ~ 5600 6500
NoConn ~ 5600 6600
NoConn ~ 5600 6700
NoConn ~ 5600 6800
NoConn ~ 5600 6900
NoConn ~ 5600 7000
NoConn ~ 5600 7100
NoConn ~ 5600 7200
NoConn ~ 5600 7300
NoConn ~ 5600 7400
NoConn ~ 5600 7500
NoConn ~ 5600 7600
Text GLabel 7650 1550 0    50   Input ~ 0
B_D0_OUT
NoConn ~ 10050 1900
NoConn ~ 8650 1650
NoConn ~ 7650 1650
Text GLabel 9750 2150 2    50   Input ~ 0
+5V
Text GLabel 9450 1900 0    50   Input ~ 0
+5V
Text GLabel 5050 6500 0    50   Input ~ 0
B_PARENA*
NoConn ~ 5050 6500
Text GLabel 7650 1250 0    50   Input ~ 0
B_RESOUT*
Text GLabel 8650 1250 2    50   Input ~ 0
RESOUT*
Text GLabel 8650 1750 2    50   Input ~ 0
WE*
Text GLabel 5950 6300 0    50   Input ~ 0
B_A14
NoConn ~ 5950 6300
NoConn ~ 8650 1450
$Comp
L 74xx:74LS125 U5
U 4 1 5F4DA0FC
P 10700 1700
F 0 "U5" H 10700 1925 50  0000 C CNN
F 1 "74LS125" H 10700 2016 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10700 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10700 1700 50  0001 C CNN
	4    10700 1700
	-1   0    0    1   
$EndComp
NoConn ~ 10400 1700
Text GLabel 10700 1450 0    50   Input ~ 0
+5V
Text GLabel 11000 1700 2    50   Input ~ 0
+5V
Text GLabel 4550 6900 0    50   Input ~ 0
B_PSEL*
Text GLabel 4550 7000 0    50   UnSpc ~ 0
B_MEM*
NoConn ~ 4550 6900
NoConn ~ 4550 7000
Text GLabel 4550 7100 0    50   Input ~ 0
B_RD*
NoConn ~ 4550 7100
NoConn ~ 7650 1450
Text Notes 7200 950  0    50   ~ 0
TODO: tie unused
Text GLabel 7650 1150 0    50   Input ~ 0
B_CLKOUT
Text GLabel 8650 1150 2    50   Input ~ 0
CLKOUT
Text GLabel 7650 5600 0    50   Input ~ 0
B_D0_OUT
Text GLabel 8650 5600 2    50   Input ~ 0
CRUOUT02
NoConn ~ 8650 3750
NoConn ~ 7650 3750
$Comp
L Device:LED D1
U 1 1 5F31B766
P 10700 4600
F 0 "D1" V 10739 4483 50  0000 R CNN
F 1 "LED" V 10648 4483 50  0000 R CNN
F 2 "LEDs:LED_D3.0mm" H 10700 4600 50  0001 C CNN
F 3 "~" H 10700 4600 50  0001 C CNN
	1    10700 4600
	0    -1   -1   0   
$EndComp
$Comp
L dk_Transistors-Bipolar-BJT-Single:2N2222A Q?
U 1 1 5F31B775
P 10600 4250
AR Path="/5EEDD9EE/5F31B775" Ref="Q?"  Part="1" 
AR Path="/5F2EEAA0/5F31B775" Ref="Q?"  Part="1" 
AR Path="/5F31B775" Ref="Q?"  Part="1" 
AR Path="/5F4BC2EA/5F31B775" Ref="Q5"  Part="1" 
AR Path="/5F2FEF9D/5F31B775" Ref="Q5"  Part="1" 
F 0 "Q5" H 10788 4303 60  0000 L CNN
F 1 "2N2222A" H 10788 4197 60  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Narrow_Oval" H 10800 4450 60  0001 L CNN
F 3 "https://my.centralsemi.com/get_document.php?cmp=1&mergetype=pd&mergepath=pd&pdf_id=2N2221A.PDF" H 10800 4550 60  0001 L CNN
F 4 "2N2222ACS-ND" H 10800 4650 60  0001 L CNN "Digi-Key_PN"
F 5 "2N2222A" H 10800 4750 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 10800 4850 60  0001 L CNN "Category"
F 7 "Transistors - Bipolar (BJT) - Single" H 10800 4950 60  0001 L CNN "Family"
F 8 "https://my.centralsemi.com/get_document.php?cmp=1&mergetype=pd&mergepath=pd&pdf_id=2N2221A.PDF" H 10800 5050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/central-semiconductor-corp/2N2222A/2N2222ACS-ND/4806845" H 10800 5150 60  0001 L CNN "DK_Detail_Page"
F 10 "TRANS NPN 40V 0.8A TO-18" H 10800 5250 60  0001 L CNN "Description"
F 11 "Central Semiconductor Corp" H 10800 5350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 10800 5450 60  0001 L CNN "Status"
	1    10600 4250
	1    0    0    -1  
$EndComp
Text GLabel 10700 4850 0    50   Input ~ 0
GND
$Comp
L Device:R R1
U 1 1 5F31B77C
P 10700 3900
F 0 "R1" H 10770 3946 50  0000 L CNN
F 1 "470" H 10770 3855 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" V 10630 3900 50  0001 C CNN
F 3 "~" H 10700 3900 50  0001 C CNN
	1    10700 3900
	1    0    0    -1  
$EndComp
Text GLabel 10700 3650 0    50   Input ~ 0
+5V
Wire Wire Line
	10700 3650 10700 3750
Wire Wire Line
	10700 4750 10700 4850
Text GLabel 10400 4250 0    50   Input ~ 0
LED0
$Comp
L Gemini:Keystone_7790_RA_Screw J?
U 1 1 5F32B364
P 10250 5450
AR Path="/5F32B364" Ref="J?"  Part="1" 
AR Path="/5F4BC2EA/5F32B364" Ref="J9"  Part="1" 
AR Path="/5F2FEF9D/5F32B364" Ref="J9"  Part="1" 
F 0 "J9" H 10650 5715 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 10650 5624 50  0000 C CNN
F 2 "Gemini:Keystone_7790_and_9203" H 10900 5550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 10900 5450 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 10900 5350 50  0001 L CNN "Description"
F 5 "7.9" H 10900 5250 50  0001 L CNN "Height"
F 6 "534-7790" H 10900 5150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 10900 5050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 10900 4950 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 10900 4850 50  0001 L CNN "Manufacturer_Part_Number"
	1    10250 5450
	1    0    0    -1  
$EndComp
$Comp
L Gemini:Keystone_7790_RA_Screw J?
U 1 1 5F32B370
P 10250 5900
AR Path="/5F32B370" Ref="J?"  Part="1" 
AR Path="/5F4BC2EA/5F32B370" Ref="J10"  Part="1" 
AR Path="/5F2FEF9D/5F32B370" Ref="J10"  Part="1" 
F 0 "J10" H 10650 6165 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 10650 6074 50  0000 C CNN
F 2 "Gemini:Keystone_7790_and_9203" H 10900 6000 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 10900 5900 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 10900 5800 50  0001 L CNN "Description"
F 5 "7.9" H 10900 5700 50  0001 L CNN "Height"
F 6 "534-7790" H 10900 5600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 10900 5500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 10900 5400 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 10900 5300 50  0001 L CNN "Manufacturer_Part_Number"
	1    10250 5900
	1    0    0    -1  
$EndComp
NoConn ~ 10250 5550
NoConn ~ 10250 5450
NoConn ~ 11050 5450
NoConn ~ 11050 5550
NoConn ~ 11050 5900
NoConn ~ 11050 6000
NoConn ~ 10250 6000
NoConn ~ 10250 5900
$EndSCHEMATC
