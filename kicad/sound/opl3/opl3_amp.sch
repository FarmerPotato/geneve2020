EESchema Schematic File Version 4
LIBS:opl3-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:TL072 U6
U 3 1 5E8694C3
P 1500 6700
F 0 "U6" H 1458 6746 50  0000 L CNN
F 1 "TL072" H 1458 6655 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1500 6700 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 1500 6700 50  0001 C CNN
	3    1500 6700
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U6
U 1 1 5E86AC62
P 3650 2600
F 0 "U6" H 3650 2967 50  0000 C CNN
F 1 "TL072" H 3650 2876 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3650 2600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3650 2600 50  0001 C CNN
	1    3650 2600
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:TL072 U6
U 2 1 5E86BDBC
P 3650 4350
F 0 "U6" H 3650 4717 50  0000 C CNN
F 1 "TL072" H 3650 4626 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 3650 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 3650 4350 50  0001 C CNN
	2    3650 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 5E86E66B
P 2500 3050
F 0 "R2" V 2295 3050 50  0000 C CNN
F 1 "100" V 2386 3050 50  0000 C CNN
F 2 "" H 2500 3050 50  0001 C CNN
F 3 "~" H 2500 3050 50  0001 C CNN
	1    2500 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 5E86EBA3
P 4650 2600
F 0 "R4" V 4445 2600 50  0000 C CNN
F 1 "1K" V 4536 2600 50  0000 C CNN
F 2 "" H 4650 2600 50  0001 C CNN
F 3 "~" H 4650 2600 50  0001 C CNN
	1    4650 2600
	0    1    1    0   
$EndComp
$Comp
L Device:CP1_Small C9
U 1 1 5E86F20B
P 2900 3050
F 0 "C9" V 2672 3050 50  0000 C CNN
F 1 "4.7uF" V 2763 3050 50  0000 C CNN
F 2 "" H 2900 3050 50  0001 C CNN
F 3 "~" H 2900 3050 50  0001 C CNN
	1    2900 3050
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 5E86F4D5
P 2900 2500
F 0 "C8" V 2671 2500 50  0000 C CNN
F 1 "2700pF" V 2762 2500 50  0000 C CNN
F 2 "" H 2900 2500 50  0001 C CNN
F 3 "~" H 2900 2500 50  0001 C CNN
	1    2900 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT_TRIM_US RV1
U 1 1 5E87038E
P 4300 3050
F 0 "RV1" V 4187 3050 50  0000 C CNN
F 1 "R_POT_TRIM_US" V 4096 3050 50  0000 C CNN
F 2 "" H 4300 3050 50  0001 C CNN
F 3 "~" H 4300 3050 50  0001 C CNN
	1    4300 3050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 2600 4300 2600
Wire Wire Line
	4150 3050 3400 3050
Wire Wire Line
	3250 3050 3250 2700
Wire Wire Line
	3250 2700 3350 2700
Wire Wire Line
	4300 2600 4300 2850
Wire Wire Line
	4300 2850 4550 2850
Wire Wire Line
	4550 2850 4550 3050
Wire Wire Line
	4550 3050 4450 3050
Connection ~ 4300 2850
Wire Wire Line
	4300 2850 4300 2900
Wire Wire Line
	4550 2600 4300 2600
Connection ~ 4300 2600
Wire Wire Line
	4750 2600 4950 2600
Wire Wire Line
	3000 3050 3250 3050
Connection ~ 3250 3050
Wire Wire Line
	2800 3050 2600 3050
Wire Wire Line
	3000 2500 3250 2500
Wire Wire Line
	2800 2500 2300 2500
Wire Wire Line
	2300 2500 2300 3050
Wire Wire Line
	2300 3050 2400 3050
Wire Wire Line
	2300 2500 2200 2500
Connection ~ 2300 2500
Text GLabel 2200 2500 0    50   Input ~ 0
GND
Wire Wire Line
	3250 2500 3250 2200
Connection ~ 3250 2500
Wire Wire Line
	3250 2500 3350 2500
Text GLabel 3250 2200 0    50   Input ~ 0
CH1L
$Comp
L Device:R_Small_US R3
U 1 1 5E8773C4
P 2500 4800
F 0 "R3" V 2295 4800 50  0000 C CNN
F 1 "100" V 2386 4800 50  0000 C CNN
F 2 "" H 2500 4800 50  0001 C CNN
F 3 "~" H 2500 4800 50  0001 C CNN
	1    2500 4800
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R5
U 1 1 5E8773CE
P 4650 4350
F 0 "R5" V 4445 4350 50  0000 C CNN
F 1 "1K" V 4536 4350 50  0000 C CNN
F 2 "" H 4650 4350 50  0001 C CNN
F 3 "~" H 4650 4350 50  0001 C CNN
	1    4650 4350
	0    1    1    0   
$EndComp
$Comp
L Device:CP1_Small C11
U 1 1 5E8773D8
P 2900 4800
F 0 "C11" V 2672 4800 50  0000 C CNN
F 1 "4.7uF" V 2763 4800 50  0000 C CNN
F 2 "" H 2900 4800 50  0001 C CNN
F 3 "~" H 2900 4800 50  0001 C CNN
	1    2900 4800
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5E8773E2
P 2900 4250
F 0 "C10" V 2671 4250 50  0000 C CNN
F 1 "2700pF" V 2762 4250 50  0000 C CNN
F 2 "" H 2900 4250 50  0001 C CNN
F 3 "~" H 2900 4250 50  0001 C CNN
	1    2900 4250
	0    1    1    0   
$EndComp
$Comp
L Device:R_POT_TRIM_US RV2
U 1 1 5E8773EC
P 4300 4800
F 0 "RV2" V 4187 4800 50  0000 C CNN
F 1 "R_POT_TRIM_US" V 4096 4800 50  0000 C CNN
F 2 "" H 4300 4800 50  0001 C CNN
F 3 "~" H 4300 4800 50  0001 C CNN
	1    4300 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 4350 4300 4350
Wire Wire Line
	4150 4800 3400 4800
Wire Wire Line
	3250 4800 3250 4450
Wire Wire Line
	3250 4450 3350 4450
Wire Wire Line
	4300 4350 4300 4600
Wire Wire Line
	4300 4600 4550 4600
Wire Wire Line
	4550 4600 4550 4800
Wire Wire Line
	4550 4800 4450 4800
Connection ~ 4300 4600
Wire Wire Line
	4300 4600 4300 4650
Wire Wire Line
	4550 4350 4300 4350
Connection ~ 4300 4350
Wire Wire Line
	4750 4350 4950 4350
Wire Wire Line
	3000 4800 3250 4800
Connection ~ 3250 4800
Wire Wire Line
	2800 4800 2600 4800
Wire Wire Line
	3000 4250 3250 4250
Wire Wire Line
	2800 4250 2300 4250
Wire Wire Line
	2300 4250 2300 4800
Wire Wire Line
	2300 4800 2400 4800
Wire Wire Line
	2300 4250 2200 4250
Connection ~ 2300 4250
Text GLabel 2200 4250 0    50   Input ~ 0
GND
Wire Wire Line
	3250 4250 3250 3950
Connection ~ 3250 4250
Wire Wire Line
	3250 4250 3350 4250
Text GLabel 3250 3950 0    50   Input ~ 0
CH1R
Text Notes 4600 5200 2    50   ~ 0
Digital Pot? I2C?
Text Notes 4700 5450 2    50   ~ 0
Amp for each connector?
$Comp
L Connector:AudioJack3_Ground_Switch J3
U 1 1 5E885B9C
P 5650 3400
F 0 "J3" H 5370 3318 50  0000 R CNN
F 1 "AudioJack3_Ground_Switch" H 5370 3227 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 5650 3400 50  0001 C CNN
F 3 "~" H 5650 3400 50  0001 C CNN
	1    5650 3400
	-1   0    0    -1  
$EndComp
Text Notes 5900 3300 0    50   ~ 0
Phono Jack\nPass-thru to RCA \nwhen phono is unplugged
Text Notes 5850 4450 2    50   ~ 0
RCA Jacks
$Comp
L Connector:AudioJack3 J4
U 1 1 5E88CD2C
P 5650 4750
F 0 "J4" H 5370 4775 50  0000 R CNN
F 1 "AudioJack3" H 5370 4684 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 5650 4750 50  0001 C CNN
F 3 "~" H 5650 4750 50  0001 C CNN
	1    5650 4750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 4350 4950 3400
Wire Wire Line
	4950 3400 5450 3400
Wire Wire Line
	4950 2600 4950 3200
Wire Wire Line
	4950 3200 5450 3200
Wire Wire Line
	5450 3300 5350 3300
Wire Wire Line
	5350 3300 5350 4650
Wire Wire Line
	5350 4650 5450 4650
Wire Wire Line
	5450 3500 5250 3500
Wire Wire Line
	5250 3500 5250 4750
Wire Wire Line
	5250 4750 5450 4750
Wire Wire Line
	5450 3700 5150 3700
Wire Wire Line
	5150 3700 5150 4850
Wire Wire Line
	5150 4850 5450 4850
Wire Wire Line
	5450 3600 3400 3600
Wire Wire Line
	3400 3600 3400 3050
Connection ~ 3400 3050
Wire Wire Line
	3400 3050 3250 3050
Wire Wire Line
	3400 3600 3400 4800
Connection ~ 3400 3600
Connection ~ 3400 4800
Wire Wire Line
	3400 4800 3250 4800
Text Notes 4800 3550 2    50   ~ 0
Where should the audio ground be
$Comp
L Connector:AudioJack3 J6
U 1 1 5F3995C6
P 8750 2500
F 0 "J6" H 8470 2525 50  0000 R CNN
F 1 "AudioJack3" H 8470 2434 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 8750 2500 50  0001 C CNN
F 3 "~" H 8750 2500 50  0001 C CNN
	1    8750 2500
	-1   0    0    -1  
$EndComp
$Comp
L Connector:AudioJack3 J7
U 1 1 5F399B14
P 8750 2900
F 0 "J7" H 8470 2925 50  0000 R CNN
F 1 "AudioJack3" H 8470 2834 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 8750 2900 50  0001 C CNN
F 3 "~" H 8750 2900 50  0001 C CNN
	1    8750 2900
	-1   0    0    -1  
$EndComp
$Comp
L Connector:AudioJack3 J8
U 1 1 5F399EA9
P 8750 3450
F 0 "J8" H 8470 3475 50  0000 R CNN
F 1 "AudioJack3" H 8470 3384 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 8750 3450 50  0001 C CNN
F 3 "~" H 8750 3450 50  0001 C CNN
	1    8750 3450
	-1   0    0    -1  
$EndComp
$Comp
L Connector:AudioJack3 J11
U 1 1 5F39A2E4
P 8750 3950
F 0 "J11" H 8470 3975 50  0000 R CNN
F 1 "AudioJack3" H 8470 3884 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_CUI_SJ1-3533NG_Horizontal" H 8750 3950 50  0001 C CNN
F 3 "~" H 8750 3950 50  0001 C CNN
	1    8750 3950
	-1   0    0    -1  
$EndComp
Text Notes 8600 5200 0    50   ~ 0
Possible analog outs:\n\nCS4344 F mix out\nCS4344 B mix out\nOPL3 dry out AB\nOPL3 dry out CD\nSN76489/TMS5220C out\nYM2608B out\n
Text Notes 5250 2900 0    50   ~ 0
To Front Panel Audio (PC97)\nHeadphone/Spkr switch is only \nrelevant on FP.\n
Wire Notes Line
	5100 5050 6850 5050
Wire Notes Line
	6850 5050 6850 3000
Wire Notes Line
	6850 3000 5100 3000
Wire Notes Line
	5100 3000 5100 5050
$EndSCHEMATC
