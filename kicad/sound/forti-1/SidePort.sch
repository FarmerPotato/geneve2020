EESchema Schematic File Version 4
LIBS:forti-1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:SidePort J?
U 1 1 609B3E67
P 5350 3550
AR Path="/609B3E67" Ref="J?"  Part="1" 
AR Path="/609B277A/609B3E67" Ref="J1"  Part="1" 
F 0 "J1" H 5350 4965 50  0000 C CNN
F 1 "SidePort" H 5350 4874 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x22_Pitch2.54mm" H 5350 3550 50  0001 C CNN
F 3 "" H 5350 3550 50  0001 C CNN
	1    5350 3550
	1    0    0    -1  
$EndComp
Text GLabel 4600 2600 0    50   Output ~ 0
B_RESET*
Text GLabel 4600 2700 0    50   Output ~ 0
B_A5
Text GLabel 4600 2800 0    50   Output ~ 0
B_A4
Text GLabel 4600 2900 0    50   Output ~ 0
B_DBIN
Text GLabel 4600 3000 0    50   Output ~ 0
B_A12
Text GLabel 4600 3100 0    50   Output ~ 0
B_LOAD*
Text GLabel 4600 3200 0    50   Output ~ 0
B_A13
Text GLabel 4600 3300 0    50   Output ~ 0
B_A7
Text GLabel 4600 3400 0    50   Output ~ 0
B_A15
Text GLabel 4600 3900 0    50   Output ~ 0
B_A6
Text GLabel 4600 4000 0    50   Output ~ 0
B_A0
Text GLabel 4600 4100 0    50   Input ~ 0
B_CRUIN
Text GLabel 4600 4200 0    50   BiDi ~ 0
B_D4
Text GLabel 4600 4300 0    50   BiDi ~ 0
B_D0
Text GLabel 4600 4400 0    50   BiDi ~ 0
B_D2
Text GLabel 4600 4500 0    50   Output ~ 0
B_IAQ
Text GLabel 6100 4100 2    50   BiDi ~ 0
B_D7
Text GLabel 6100 4200 2    50   BiDi ~ 0
B_D6
Text GLabel 6100 4300 2    50   BiDi ~ 0
B_D5
Text GLabel 6100 4400 2    50   BiDi ~ 0
B_D1
Text GLabel 6100 4500 2    50   BiDi ~ 0
B_D3
Text GLabel 6100 4600 2    50   Input ~ 0
B_AUDIOIN
Text GLabel 6100 2500 2    50   Output ~ 0
B_SBE
Text GLabel 6100 2600 2    50   Input ~ 0
B_EXTINT*
Text GLabel 6100 2700 2    50   Output ~ 0
B_A10
Text GLabel 6100 2800 2    50   Output ~ 0
B_A11
Text GLabel 6100 2900 2    50   Output ~ 0
B_A3
Text GLabel 6100 3000 2    50   Input ~ 0
B_READY
Text GLabel 6100 3100 2    50   Output ~ 0
B_A8
Text GLabel 6100 3200 2    50   Output ~ 0
B_A14
Text GLabel 6100 3300 2    50   Output ~ 0
B_A9
Text GLabel 6100 3400 2    50   Output ~ 0
B_A2
Text GLabel 6100 3500 2    50   Output ~ 0
B_CRUCLK*
Text GLabel 6100 3600 2    50   Output ~ 0
B_PHI3*
Text GLabel 6100 3700 2    50   Output ~ 0
B_WE*
Text GLabel 6100 3800 2    50   Output ~ 0
B_MBE*
Text GLabel 6100 3900 2    50   Output ~ 0
B_A1
Text GLabel 6100 4000 2    50   Output ~ 0
B_MEMEN*
Text GLabel 4600 4600 0    50   Output ~ 0
B_VDD
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 609B6557
P 3600 2500
F 0 "#FLG0101" H 3600 2575 50  0001 C CNN
F 1 "PWR_FLAG" H 3600 2673 50  0000 C CNN
F 2 "" H 3600 2500 50  0001 C CNN
F 3 "~" H 3600 2500 50  0001 C CNN
	1    3600 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 609B6741
P 3100 2500
F 0 "#PWR0101" H 3100 2350 50  0001 C CNN
F 1 "+5V" H 3115 2673 50  0000 C CNN
F 2 "" H 3100 2500 50  0001 C CNN
F 3 "" H 3100 2500 50  0001 C CNN
	1    3100 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 2500 3600 2500
Connection ~ 3600 2500
Wire Wire Line
	3600 2500 3100 2500
$Comp
L power:GND #PWR0102
U 1 1 609B6B35
P 3100 3800
F 0 "#PWR0102" H 3100 3550 50  0001 C CNN
F 1 "GND" H 3105 3627 50  0000 C CNN
F 2 "" H 3100 3800 50  0001 C CNN
F 3 "" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3800 4600 3800
Wire Wire Line
	4600 3700 3100 3700
Wire Wire Line
	4600 3600 3100 3600
Text Notes 7250 2300 0    50   ~ 0
Unused Pins
Text GLabel 7350 2400 2    50   Output ~ 0
B_SBE
Text GLabel 7350 2500 2    50   Input ~ 0
B_EXTINT*
Text GLabel 7350 2600 2    50   Output ~ 0
B_PHI3*
Text GLabel 7350 2700 2    50   Output ~ 0
B_MBE*
Text GLabel 7350 2800 2    50   Output ~ 0
B_LOAD*
Text GLabel 7350 2900 2    50   Output ~ 0
B_IAQ
Text GLabel 7350 3000 2    50   Output ~ 0
B_VDD
NoConn ~ 7350 2400
NoConn ~ 7350 2500
NoConn ~ 7350 2600
NoConn ~ 7350 2700
NoConn ~ 7350 2800
NoConn ~ 7350 2900
NoConn ~ 7350 3000
Text GLabel 7350 3100 2    50   Input ~ 0
B_CRUIN
NoConn ~ 7350 3100
Wire Wire Line
	3100 3500 4600 3500
Text GLabel 3100 3500 0    50   Input ~ 0
GND
Text GLabel 3100 3600 0    50   Input ~ 0
GND
Text GLabel 3100 3700 0    50   Input ~ 0
GND
$EndSCHEMATC
