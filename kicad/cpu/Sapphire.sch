EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:TMS99105 U1
U 1 1 5E0DF377
P 4950 2400
F 0 "U1" H 4950 3981 50  0000 C CNN
F 1 "TMS99105" H 4950 3890 50  0000 C CNN
F 2 "Housings_DIP:DIP-40_W15.24mm_Socket_LongPads" H 4950 2400 50  0001 C CNN
F 3 "" H 4950 2400 50  0001 C CNN
	1    4950 2400
	1    0    0    -1  
$EndComp
Text GLabel 4100 2300 0    50   Input ~ 0
IC0
Text GLabel 4100 2400 0    50   Input ~ 0
IC1
Text GLabel 4100 2500 0    50   Input ~ 0
IC2
Text GLabel 4100 2600 0    50   Input ~ 0
IC3
Text GLabel 4100 2100 0    50   Input ~ 0
INTREQ*
Text GLabel 4100 1500 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 4100 1700 0    50   Input ~ 0
RESET*
Text GLabel 5800 2200 2    50   Input ~ 0
ALATCH
Text GLabel 5800 2100 2    50   Input ~ 0
CLKOUT
Text GLabel 4100 1900 0    50   Input ~ 0
HOLD*
Text GLabel 4100 2000 0    50   Input ~ 0
READY
Text GLabel 4100 2200 0    50   Input ~ 0
NMI*
Text GLabel 4100 1600 0    50   Input ~ 0
RD*
Text GLabel 5800 1500 2    50   Input ~ 0
MEM*
$Comp
L Device:Crystal Y1
U 1 1 5E1B81F8
P 10350 1800
F 0 "Y1" V 10304 1931 50  0000 L CNN
F 1 "24 MHz" V 10395 1931 50  0000 L CNN
F 2 "Crystals:Crystal_HC49-U_Vertical" H 10350 1800 50  0001 C CNN
F 3 "~" H 10350 1800 50  0001 C CNN
	1    10350 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	9700 1800 10200 1800
Wire Wire Line
	10200 1800 10200 1950
Wire Wire Line
	9700 1700 10200 1700
Wire Wire Line
	10200 1700 10200 1650
Wire Wire Line
	10200 1650 10350 1650
Text GLabel 5000 1000 2    50   Input ~ 0
VCC
Text GLabel 8400 1200 0    50   Input ~ 0
GND
Text Notes 7500 7500 0    50   ~ 0
Prototype 99105 Board with BlackIce for FPGA
Text Notes 8200 7650 0    50   ~ 0
12/4/2019
Text Notes 10700 7650 0    50   ~ 0
0
Text Notes 10150 7500 0    50   ~ 0
Erik Olson (FarmerPotato)
Text Notes 3850 1250 0    50   ~ 0
Tie RESET*,APP* for \nMacrostore Prototyping
$Comp
L Device:R R4
U 1 1 5E21BA3B
P 6900 1550
F 0 "R4" V 6900 1400 50  0000 C CNN
F 1 "10K" V 6900 1550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6830 1550 50  0001 C CNN
F 3 "~" H 6900 1550 50  0001 C CNN
	1    6900 1550
	0    -1   -1   0   
$EndComp
Text GLabel 6750 1450 0    50   Input ~ 0
VCC
$Comp
L Device:R R3
U 1 1 5E22133B
P 6900 1450
F 0 "R3" V 6900 1300 50  0000 C CNN
F 1 "10K" V 6900 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6830 1450 50  0001 C CNN
F 3 "~" H 6900 1450 50  0001 C CNN
	1    6900 1450
	0    -1   -1   0   
$EndComp
Text Notes 7000 7150 0    100  ~ 0
Geneve2020 Test Board\nCPU, Backplane to CRU and Memory\nFPGA for  RAM, Macrostore, CRU peripherals. \nFPGA hosts VDP, Sound Bus.
Text GLabel 5800 1600 2    50   Input ~ 0
BST1
Text GLabel 5800 1700 2    50   Input ~ 0
BST2
Text GLabel 5800 1800 2    50   Input ~ 0
BST3
Text GLabel 5800 1900 2    50   Input ~ 0
XTAL1
Text GLabel 5800 2000 2    50   Input ~ 0
XTAL2
$Comp
L Device:C C5
U 1 1 5E30E5FA
P 6200 7400
F 0 "C5" H 6150 7650 50  0000 L CNN
F 1 "0.1uF" H 6100 7150 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6238 7250 50  0001 C CNN
F 3 "~" H 6200 7400 50  0001 C CNN
	1    6200 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E312454
P 3350 7400
F 0 "C1" H 3300 7650 50  0000 L CNN
F 1 "0.1uF" H 3250 7150 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3388 7250 50  0001 C CNN
F 3 "~" H 3350 7400 50  0001 C CNN
	1    3350 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E312468
P 5700 7400
F 0 "C3" H 5650 7650 50  0000 L CNN
F 1 "0.1uF" H 5600 7150 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5738 7250 50  0001 C CNN
F 3 "~" H 5700 7400 50  0001 C CNN
	1    5700 7400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5E312472
P 5950 7400
F 0 "C4" H 5900 7650 50  0000 L CNN
F 1 "0.1uF" H 5850 7150 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 5988 7250 50  0001 C CNN
F 3 "~" H 5950 7400 50  0001 C CNN
	1    5950 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 7250 5950 7250
Connection ~ 5950 7250
Wire Wire Line
	5950 7250 6200 7250
Connection ~ 3350 7550
Wire Wire Line
	5700 7550 5950 7550
Connection ~ 5950 7550
Wire Wire Line
	5950 7550 6200 7550
Text GLabel 2650 7250 0    50   Input ~ 0
VCC
Text GLabel 2650 7550 0    50   Input ~ 0
GND
Wire Wire Line
	4900 3800 5000 3800
Connection ~ 5000 3800
Wire Wire Line
	5000 3800 5100 3800
Text GLabel 5100 3800 2    50   Input ~ 0
GND
Text GLabel 5950 6500 2    50   Input ~ 0
BALATCH
$Comp
L Device:R R5
U 1 1 5E3F492C
P 6900 1650
F 0 "R5" V 6900 1500 50  0000 C CNN
F 1 "10K" V 6900 1650 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6830 1650 50  0001 C CNN
F 3 "~" H 6900 1650 50  0001 C CNN
	1    6900 1650
	0    -1   -1   0   
$EndComp
Text Notes 8750 3550 0    50   ~ 0
To Memory
Text GLabel 3750 5700 0    50   Input ~ 0
ALATCH
Text GLabel 2800 6300 2    50   Input ~ 0
GND
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC245AN U4
U 1 1 5E11C9FF
P 2800 5100
F 0 "U4" H 2700 5603 60  0000 C CNN
F 1 "SN74LVC245AN" H 2700 5497 60  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 3000 5300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 3000 5400 60  0001 L CNN
F 4 "296-8503-5-ND" H 3000 5500 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC245AN" H 3000 5600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 3000 5700 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 3000 5800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 3000 5900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC245AN/296-8503-5-ND/377483" H 3000 6000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC TXRX NON-INVERT 3.6V 20DIP" H 3000 6100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 3000 6200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3000 6300 60  0001 L CNN "Status"
	1    2800 5100
	1    0    0    -1  
$EndComp
Text GLabel 1500 4800 2    50   Input ~ 0
3V3
Text GLabel 1500 6300 2    50   Input ~ 0
GND
Wire Wire Line
	1100 6300 1100 6000
Wire Wire Line
	1500 6300 1100 6300
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC245AN U5
U 1 1 5E129804
P 4150 5100
F 0 "U5" H 4050 5603 60  0000 C CNN
F 1 "SN74LVC245AN" H 4050 5497 60  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 4350 5300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 4350 5400 60  0001 L CNN
F 4 "296-8503-5-ND" H 4350 5500 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC245AN" H 4350 5600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 4350 5700 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 4350 5800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 4350 5900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC245AN/296-8503-5-ND/377483" H 4350 6000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC TXRX NON-INVERT 3.6V 20DIP" H 4350 6100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 4350 6200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 4350 6300 60  0001 L CNN "Status"
	1    4150 5100
	1    0    0    -1  
$EndComp
Text GLabel 1700 5200 2    50   Input ~ 0
BAD0
Text GLabel 1700 5300 2    50   Input ~ 0
BAD1
Text GLabel 1700 5400 2    50   Input ~ 0
BAD2
Text GLabel 1700 5500 2    50   Input ~ 0
BAD3
Text GLabel 1700 5600 2    50   Input ~ 0
BAD4
Text GLabel 1700 5700 2    50   Input ~ 0
BAD5
Text GLabel 1700 5800 2    50   Input ~ 0
BAD6
Text GLabel 1700 5900 2    50   Input ~ 0
BAD7
Wire Wire Line
	2400 6000 2400 6300
Wire Wire Line
	2400 6300 2800 6300
Text GLabel 2800 4800 2    50   Input ~ 0
3V3
Text GLabel 4150 4800 2    50   Input ~ 0
3V3
Wire Wire Line
	3750 6000 3750 6300
Wire Wire Line
	3750 6300 4150 6300
Text GLabel 4150 6300 2    50   Input ~ 0
GND
Text GLabel 3750 5400 0    50   Input ~ 0
BST1
Text GLabel 3750 5500 0    50   Input ~ 0
BST2
Text GLabel 3750 5600 0    50   Input ~ 0
BST3
Text GLabel 3750 5300 0    50   Input ~ 0
MEM*
Text GLabel 3750 5100 0    50   Input ~ 0
RD*
Text Notes 2250 6200 2    50   ~ 0
DIR high: A to B\nDIR low:  B to A
$Comp
L dk_Logic-Buffers-Drivers-Receivers-Transceivers:SN74LVC245AN U3
U 1 1 5E109034
P 1500 5100
F 0 "U3" H 1400 5603 60  0000 C CNN
F 1 "SN74LVC245AN" H 1400 5497 60  0000 C CNN
F 2 "Package_SO:SOIC-20W_7.5x12.8mm_P1.27mm" H 1700 5300 60  0001 L CNN
F 3 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 1700 5400 60  0001 L CNN
F 4 "296-8503-5-ND" H 1700 5500 60  0001 L CNN "Digi-Key_PN"
F 5 "SN74LVC245AN" H 1700 5600 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 1700 5700 60  0001 L CNN "Category"
F 7 "Logic - Buffers, Drivers, Receivers, Transceivers" H 1700 5800 60  0001 L CNN "Family"
F 8 "http://www.ti.com/general/docs/suppproductinfo.tsp?distId=10&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Fsn74lvc245a" H 1700 5900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/texas-instruments/SN74LVC245AN/296-8503-5-ND/377483" H 1700 6000 60  0001 L CNN "DK_Detail_Page"
F 10 "IC TXRX NON-INVERT 3.6V 20DIP" H 1700 6100 60  0001 L CNN "Description"
F 11 "Texas Instruments" H 1700 6200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1700 6300 60  0001 L CNN "Status"
	1    1500 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5E1551E6
P 2300 4800
F 0 "R6" H 2359 4846 50  0000 L CNN
F 1 "1K" H 2359 4755 50  0000 L CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 2300 4800 50  0001 C CNN
F 3 "~" H 2300 4800 50  0001 C CNN
	1    2300 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1100 5900 1050 5900
Wire Wire Line
	2350 5900 2400 5900
Text GLabel 2050 4800 1    50   Input ~ 0
EXBAE
Text GLabel 4350 5500 2    50   Input ~ 0
BBST1
Text GLabel 4350 5600 2    50   Input ~ 0
BBST2
Text GLabel 4350 5700 2    50   Input ~ 0
BBST3
Text GLabel 4350 5800 2    50   Input ~ 0
BALATCH
Text GLabel 4350 5400 2    50   Input ~ 0
BMEM*
Text GLabel 4350 5200 2    50   Input ~ 0
BRD*
Text GLabel 4350 5300 2    50   Input ~ 0
BWE*
Text Notes 3700 6500 0    50   ~ 0
What to do with\nunused 245 gate?
Text Notes 3050 4900 0    50   ~ 0
What happens when 3V3 \nheader is not connected?\n
Text GLabel 3000 5200 2    50   Input ~ 0
BAD15
Text GLabel 3000 5300 2    50   Input ~ 0
BAD14
Text GLabel 3000 5400 2    50   Input ~ 0
BAD13
Text GLabel 3000 5500 2    50   Input ~ 0
BAD12
Text GLabel 3000 5600 2    50   Input ~ 0
BAD11
Text GLabel 3000 5700 2    50   Input ~ 0
BAD10
Text GLabel 3000 5800 2    50   Input ~ 0
BAD9
Text GLabel 3000 5900 2    50   Input ~ 0
BAD8
Text GLabel 3750 5900 0    50   Input ~ 0
3V3
$Comp
L Device:C_Small C13
U 1 1 5E3EC6DF
P 10800 1650
F 0 "C13" V 10571 1650 50  0000 C CNN
F 1 "5pF" V 10662 1650 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 10800 1650 50  0001 C CNN
F 3 "~" H 10800 1650 50  0001 C CNN
	1    10800 1650
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C14
U 1 1 5E3ED79B
P 10800 1950
F 0 "C14" V 10571 1950 50  0000 C CNN
F 1 "5pF" V 10662 1950 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 10800 1950 50  0001 C CNN
F 3 "~" H 10800 1950 50  0001 C CNN
	1    10800 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	10700 1650 10350 1650
Connection ~ 10350 1650
Wire Wire Line
	10900 1650 10900 1800
Text GLabel 10950 1800 2    50   Input ~ 0
GND
$Comp
L Connector:Barrel_Jack J5
U 1 1 5E0D95DF
P 10350 900
F 0 "J5" H 10100 1200 50  0000 C CNN
F 1 "Barrel_Jack" H 10350 1100 50  0000 C CNN
F 2 "Connect:Barrel_Jack_CUI_PJ-102AH" H 10400 860 50  0001 C CNN
F 3 "~" H 10400 860 50  0001 C CNN
	1    10350 900 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 5E0FFED4
P 10650 800
F 0 "#PWR0101" H 10650 650 50  0001 C CNN
F 1 "VCC" H 10667 973 50  0000 C CNN
F 2 "" H 10650 800 50  0001 C CNN
F 3 "" H 10650 800 50  0001 C CNN
	1    10650 800 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E100A36
P 10650 1000
F 0 "#PWR0102" H 10650 750 50  0001 C CNN
F 1 "GND" H 10655 827 50  0000 C CNN
F 2 "" H 10650 1000 50  0001 C CNN
F 3 "" H 10650 1000 50  0001 C CNN
	1    10650 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5900 1050 6500
Wire Wire Line
	2350 5900 2350 6500
Wire Wire Line
	2800 4800 2400 4800
Wire Wire Line
	1050 6500 2350 6500
Wire Wire Line
	2050 5900 2350 5900
Connection ~ 2350 5900
Wire Wire Line
	2200 4800 2050 4800
Wire Wire Line
	2050 4800 2050 5900
Text GLabel 7150 1650 2    50   Input ~ 0
HOLD*
Text GLabel 7150 1450 2    50   Input ~ 0
READY
Text GLabel 7150 1550 2    50   Input ~ 0
INTREQ*
Wire Wire Line
	7050 1650 7150 1650
Wire Wire Line
	7150 1450 7050 1450
Wire Wire Line
	7050 1550 7150 1550
Text GLabel 6750 1550 0    50   Input ~ 0
VCC
Wire Wire Line
	4100 1800 4100 1700
Text Notes 10050 550  0    50   ~ 0
Regulated +5 from bench
Text GLabel 10650 800  2    50   Input ~ 0
VCC
Text GLabel 10650 1000 2    50   Input ~ 0
GND
Text GLabel 9700 1700 0    50   Input ~ 0
XTAL1
Text GLabel 9700 1800 0    50   Input ~ 0
XTAL2
Wire Wire Line
	10950 1800 10900 1800
Connection ~ 10900 1800
Wire Wire Line
	10900 1800 10900 1950
$Comp
L Device:C_Small C16
U 1 1 5E2EE14D
P 8650 1100
F 0 "C16" V 8421 1100 50  0000 C CNN
F 1 "4.7uF" V 8512 1100 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8650 1100 50  0001 C CNN
F 3 "~" H 8650 1100 50  0001 C CNN
	1    8650 1100
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5E2EB7F6
P 8400 1000
F 0 "SW2" H 8400 1150 50  0000 C CNN
F 1 "SW_SPST" H 8400 866 50  0001 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_TH_Tactile_Omron_B3F-10xx" H 8400 1000 50  0001 C CNN
F 3 "~" H 8400 1000 50  0001 C CNN
	1    8400 1000
	0    -1   1    0   
$EndComp
Wire Wire Line
	8400 1200 8650 1200
Text GLabel 8000 800  0    50   Input ~ 0
VCC
$Comp
L Device:R R2
U 1 1 5E1C62F0
P 8150 800
F 0 "R2" V 8150 650 50  0000 C CNN
F 1 "10K" V 8150 800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 8080 800 50  0001 C CNN
F 3 "~" H 8150 800 50  0001 C CNN
	1    8150 800 
	0    -1   -1   0   
$EndComp
Text GLabel 8650 800  2    50   Input ~ 0
NMI*
Wire Wire Line
	8300 800  8400 800 
Connection ~ 8400 800 
Wire Wire Line
	8400 800  8650 800 
Wire Wire Line
	8650 1000 8650 800 
Text GLabel 6750 1650 0    50   Input ~ 0
VCC
$Comp
L power:+3V3 #PWR0103
U 1 1 5E101585
P 5150 7250
F 0 "#PWR0103" H 5150 7100 50  0001 C CNN
F 1 "+3V3" H 5150 7400 50  0000 C CNN
F 2 "" H 5150 7250 50  0001 C CNN
F 3 "" H 5150 7250 50  0001 C CNN
	1    5150 7250
	1    0    0    -1  
$EndComp
Text GLabel 5450 5400 0    50   Input ~ 0
BAD14
Text GLabel 5450 5500 0    50   Input ~ 0
BAD12
Text GLabel 5450 5600 0    50   Input ~ 0
BAD10
Text GLabel 5450 5700 0    50   Input ~ 0
BAD8
Text GLabel 5950 5700 2    50   Input ~ 0
BAD9
Text GLabel 5950 5600 2    50   Input ~ 0
BAD11
Text GLabel 5950 5500 2    50   Input ~ 0
BAD13
Text GLabel 5950 5400 2    50   Input ~ 0
BAD15
Text GLabel 5450 6100 0    50   Input ~ 0
GND
Text GLabel 5450 5300 0    50   Input ~ 0
GND
Text GLabel 5950 6000 2    50   Input ~ 0
3V3
Text GLabel 5950 5200 2    50   Input ~ 0
3V3
Text GLabel 5450 6200 0    50   Input ~ 0
BWE*
Text GLabel 5950 6200 2    50   Input ~ 0
BRD*
Text GLabel 5950 6300 2    50   Input ~ 0
BMEM*
Text GLabel 5450 6500 0    50   Input ~ 0
EXBAE
Text GLabel 5950 4750 2    50   Input ~ 0
BAD2
Text GLabel 5950 4850 2    50   Input ~ 0
BAD4
Text GLabel 5950 4950 2    50   Input ~ 0
BAD6
Text GLabel 5450 4950 0    50   Input ~ 0
BAD7
Text GLabel 5450 4850 0    50   Input ~ 0
BAD5
Text GLabel 5450 4750 0    50   Input ~ 0
BAD3
Text GLabel 5450 4650 0    50   Input ~ 0
BAD1
Text GLabel 5950 4650 2    50   Input ~ 0
BAD0
$Comp
L Connector_Generic:Conn_02x06_Odd_Even PMOD9/10
U 1 1 5E0E0AF6
P 5650 5400
F 0 "PMOD9/10" H 5700 5725 50  0000 C CNN
F 1 "Conn_02x06_Top_Bottom" H 5700 5726 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x06_Pitch2.54mm" H 5650 5400 50  0001 C CNN
F 3 "~" H 5650 5400 50  0001 C CNN
	1    5650 5400
	1    0    0    -1  
$EndComp
Text GLabel 5950 5300 2    50   Input ~ 0
GND
Text GLabel 5450 5200 0    50   Input ~ 0
3V3
Text Notes 5350 4250 0    50   ~ 0
TO FPGA
Text GLabel 5950 4550 2    50   Input ~ 0
GND
Text GLabel 5450 4550 0    50   Input ~ 0
GND
Text GLabel 5950 4450 2    50   Input ~ 0
3V3
Text GLabel 5450 4450 0    50   Input ~ 0
3V3
$Comp
L Connector_Generic:Conn_02x06_Odd_Even PMOD11/12
U 1 1 5E0E4A40
P 5650 4650
F 0 "PMOD11/12" H 5700 4975 50  0000 C CNN
F 1 "Conn_02x06_Top_Bottom" H 5700 4976 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x06_Pitch2.54mm" H 5650 4650 50  0001 C CNN
F 3 "~" H 5650 4650 50  0001 C CNN
	1    5650 4650
	1    0    0    -1  
$EndComp
Text Notes 6300 6250 0    50   ~ 0
From FPGA
Text GLabel 5950 6100 2    50   Input ~ 0
GND
Text GLabel 5450 6000 0    50   Input ~ 0
3V3
$Comp
L Connector_Generic:Conn_02x06_Odd_Even PMOD7/8
U 1 1 5E38B11A
P 5650 6200
F 0 "PMOD7/8" H 5700 6525 50  0000 C CNN
F 1 "Conn_02x06_Top_Bottom" H 5700 6526 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x06_Pitch2.54mm" H 5650 6200 50  0001 C CNN
F 3 "~" H 5650 6200 50  0001 C CNN
	1    5650 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 1950 10350 1950
Connection ~ 10350 1950
Wire Wire Line
	10350 1950 10700 1950
Text GLabel 7150 1200 0    50   Input ~ 0
GND
$Comp
L Device:C_Small C15
U 1 1 5E3A080D
P 7400 1100
F 0 "C15" V 7171 1100 50  0000 C CNN
F 1 "4.7uF" V 7262 1100 50  0000 C CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 7400 1100 50  0001 C CNN
F 3 "~" H 7400 1100 50  0001 C CNN
	1    7400 1100
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5E3A0817
P 7150 1000
F 0 "SW1" H 7150 1150 50  0000 C CNN
F 1 "SW_SPST" H 7150 866 50  0001 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_TH_Tactile_Omron_B3F-10xx" H 7150 1000 50  0001 C CNN
F 3 "~" H 7150 1000 50  0001 C CNN
	1    7150 1000
	0    -1   1    0   
$EndComp
Wire Wire Line
	7150 1200 7400 1200
Text GLabel 6750 800  0    50   Input ~ 0
VCC
$Comp
L Device:R R1
U 1 1 5E3A0823
P 6900 800
F 0 "R1" V 6900 650 50  0000 C CNN
F 1 "10K" V 6900 800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 6830 800 50  0001 C CNN
F 3 "~" H 6900 800 50  0001 C CNN
	1    6900 800 
	0    -1   -1   0   
$EndComp
Text GLabel 7400 800  2    50   Input ~ 0
RESET*
Wire Wire Line
	7050 800  7150 800 
Connection ~ 7150 800 
Wire Wire Line
	7150 800  7400 800 
Wire Wire Line
	7400 1000 7400 800 
$Sheet
S 8400 2900 1000 550 
U 5E8FF889
F0 "Backplane" 50
F1 "Backplane.sch" 50
$EndSheet
Text GLabel 5800 2300 2    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 2400 5800 0    50   Input ~ 0
AD8
Text GLabel 2400 5100 0    50   Input ~ 0
PSEL*_AD15_OUT
Text GLabel 2400 5700 0    50   Input ~ 0
AD9
Text GLabel 2400 5600 0    50   Input ~ 0
AD10
Text GLabel 2400 5500 0    50   Input ~ 0
AD11
Text GLabel 2400 5400 0    50   Input ~ 0
AD12
Text GLabel 2400 5300 0    50   Input ~ 0
AD13
Text GLabel 2400 5200 0    50   Input ~ 0
AD14
NoConn ~ 3750 5800
NoConn ~ 4350 5900
Text GLabel 1100 5500 0    50   Input ~ 0
AD4
Text GLabel 1100 5200 0    50   Input ~ 0
AD1
Text GLabel 1100 5300 0    50   Input ~ 0
AD2
Text GLabel 1100 5400 0    50   Input ~ 0
AD3
Text GLabel 1100 5600 0    50   Input ~ 0
AD5
Text GLabel 1100 5700 0    50   Input ~ 0
AD6
Text GLabel 1100 5800 0    50   Input ~ 0
AD7
Text GLabel 3750 5200 0    50   Input ~ 0
WE*_IOCLK*
Text GLabel 4100 2800 0    50   Input ~ 0
AD0_IN
Text GLabel 4100 3200 0    50   Input ~ 0
AD4
Text GLabel 4100 2900 0    50   Input ~ 0
AD1
Text GLabel 4100 3000 0    50   Input ~ 0
AD2
Text GLabel 4100 3100 0    50   Input ~ 0
AD3
Text GLabel 5800 3300 2    50   Input ~ 0
AD5
Text GLabel 5800 3200 2    50   Input ~ 0
AD6
Text GLabel 5800 3100 2    50   Input ~ 0
AD7
Text GLabel 5800 3000 2    50   Input ~ 0
AD8
Text GLabel 5800 2900 2    50   Input ~ 0
AD9
Text GLabel 5800 2800 2    50   Input ~ 0
AD10
Text GLabel 5800 2700 2    50   Input ~ 0
AD11
Text GLabel 5800 2600 2    50   Input ~ 0
AD12
Text GLabel 5800 2500 2    50   Input ~ 0
AD13
Text GLabel 5800 2400 2    50   Input ~ 0
AD14
$Comp
L power:GND #PWR0104
U 1 1 5E9E3F7C
P 3350 7550
F 0 "#PWR0104" H 3350 7300 50  0001 C CNN
F 1 "GND" H 3355 7377 50  0000 C CNN
F 2 "" H 3350 7550 50  0001 C CNN
F 3 "" H 3350 7550 50  0001 C CNN
	1    3350 7550
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E9E4851
P 3150 7250
F 0 "#FLG0101" H 3150 7325 50  0001 C CNN
F 1 "PWR_FLAG" H 3150 7423 50  0000 C CNN
F 2 "" H 3150 7250 50  0001 C CNN
F 3 "~" H 3150 7250 50  0001 C CNN
	1    3150 7250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0105
U 1 1 5E9E353B
P 2850 7250
F 0 "#PWR0105" H 2850 7100 50  0001 C CNN
F 1 "+5V" H 2850 7400 50  0000 C CNN
F 2 "" H 2850 7250 50  0001 C CNN
F 3 "" H 2850 7250 50  0001 C CNN
	1    2850 7250
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E9E617B
P 2850 7550
F 0 "#FLG0102" H 2850 7625 50  0001 C CNN
F 1 "PWR_FLAG" H 2850 7723 50  0000 C CNN
F 2 "" H 2850 7550 50  0001 C CNN
F 3 "~" H 2850 7550 50  0001 C CNN
	1    2850 7550
	1    0    0    -1  
$EndComp
Connection ~ 3150 7250
Wire Wire Line
	3150 7250 3350 7250
Wire Wire Line
	2850 7250 3150 7250
Wire Wire Line
	2650 7250 2850 7250
Connection ~ 2850 7250
Wire Wire Line
	2650 7550 2850 7550
Connection ~ 2850 7550
Wire Wire Line
	2850 7550 3350 7550
NoConn ~ 4100 2700
Text GLabel 5450 6400 0    50   Input ~ 0
BBST3
Text GLabel 5950 6400 2    50   Input ~ 0
BBST2
Text GLabel 5450 6300 0    50   Input ~ 0
BBST1
Text GLabel 1100 5100 0    50   Input ~ 0
AD0_IN
Text GLabel 5050 7250 0    50   Input ~ 0
3V3
Text GLabel 5050 7550 0    50   Input ~ 0
GND
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5EBF8B5C
P 5450 7250
F 0 "#FLG0103" H 5450 7325 50  0001 C CNN
F 1 "PWR_FLAG" H 5450 7423 50  0000 C CNN
F 2 "" H 5450 7250 50  0001 C CNN
F 3 "~" H 5450 7250 50  0001 C CNN
	1    5450 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 7250 5150 7250
Connection ~ 5700 7250
Wire Wire Line
	5050 7550 5700 7550
Connection ~ 5700 7550
Connection ~ 5150 7250
Connection ~ 5450 7250
Wire Wire Line
	5450 7250 5700 7250
Wire Wire Line
	5150 7250 5450 7250
$Comp
L Device:LED D1
U 1 1 5EC33B81
P 4050 7250
F 0 "D1" H 4043 6995 50  0000 C CNN
F 1 "LED" H 4043 7086 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 4050 7250 50  0001 C CNN
F 3 "~" H 4050 7250 50  0001 C CNN
	1    4050 7250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5EC3764F
P 3800 7250
F 0 "R7" V 3604 7250 50  0000 C CNN
F 1 "R_Small" V 3695 7250 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" H 3800 7250 50  0001 C CNN
F 3 "~" H 3800 7250 50  0001 C CNN
	1    3800 7250
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 7250 3700 7250
Connection ~ 3350 7250
Wire Wire Line
	4200 7250 4200 7550
Wire Wire Line
	4200 7550 3350 7550
Text Notes 3700 7400 0    50   ~ 0
Power Light
$EndSCHEMATC
