EESchema Schematic File Version 4
LIBS:Steven-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:Keystone_7790_RA_Screw J5
U 1 1 5F142B37
P 2400 1800
F 0 "J5" H 2800 1435 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 2800 1526 50  0000 C CNN
F 2 "Gemini:Keystone_7790_RA_Screw" H 3050 1900 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 3050 1800 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 3050 1700 50  0001 L CNN "Description"
F 5 "7.9" H 3050 1600 50  0001 L CNN "Height"
F 6 "534-7790" H 3050 1500 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 3050 1400 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 3050 1300 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 3050 1200 50  0001 L CNN "Manufacturer_Part_Number"
	1    2400 1800
	1    0    0    1   
$EndComp
$Comp
L Gemini:Keystone_7790_RA_Screw J4
U 1 1 5F14303C
P 2400 2450
F 0 "J4" H 2800 2085 50  0000 C CNN
F 1 "Keystone_7790_RA_Screw" H 2800 2176 50  0000 C CNN
F 2 "Gemini:Keystone_7790_RA_Screw" H 3050 2550 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/7790.pdf" H 3050 2450 50  0001 L CNN
F 4 "Terminals KEYSTONE PCVB METRIC SCREW TRM HORIZ" H 3050 2350 50  0001 L CNN "Description"
F 5 "7.9" H 3050 2250 50  0001 L CNN "Height"
F 6 "534-7790" H 3050 2150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=534-7790" H 3050 2050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Keystone Electronics" H 3050 1950 50  0001 L CNN "Manufacturer_Name"
F 9 "7790" H 3050 1850 50  0001 L CNN "Manufacturer_Part_Number"
	1    2400 2450
	1    0    0    1   
$EndComp
$Comp
L Device:LED_Small D1
U 1 1 5F1438F8
P 4250 2400
F 0 "D1" V 4296 2332 50  0000 R CNN
F 1 "LED_Small" V 4205 2332 50  0000 R CNN
F 2 "LEDs:LED_D3.0mm" V 4250 2400 50  0001 C CNN
F 3 "~" V 4250 2400 50  0001 C CNN
	1    4250 2400
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5F144516
P 4250 2200
F 0 "R9" H 4309 2246 50  0000 L CNN
F 1 "470" H 4309 2155 50  0000 L CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" H 4250 2200 50  0001 C CNN
F 3 "~" H 4250 2200 50  0001 C CNN
	1    4250 2200
	1    0    0    -1  
$EndComp
Text GLabel 4250 2000 0    50   Input ~ 0
+5V
Text GLabel 4250 2650 0    50   Input ~ 0
GND
Wire Wire Line
	4250 2000 4250 2100
Wire Wire Line
	4250 2500 4250 2650
Text GLabel 2950 3450 0    50   Input ~ 0
GND
$Comp
L Device:C_Small C?
U 1 1 5F193DDB
P 3200 3350
AR Path="/5F193DDB" Ref="C?"  Part="1" 
AR Path="/5F142239/5F193DDB" Ref="C21"  Part="1" 
F 0 "C21" V 2971 3350 50  0000 C CNN
F 1 "0.1uF" V 3062 3350 50  0000 C CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 3200 3350 50  0001 C CNN
F 3 "~" H 3200 3350 50  0001 C CNN
	1    3200 3350
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5F193DE1
P 2950 3250
AR Path="/5F193DE1" Ref="SW?"  Part="1" 
AR Path="/5F142239/5F193DE1" Ref="SW2"  Part="1" 
F 0 "SW2" H 2950 3400 50  0000 C CNN
F 1 "NMI" H 2950 3116 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 2950 3250 50  0001 C CNN
F 3 "~" H 2950 3250 50  0001 C CNN
	1    2950 3250
	0    -1   1    0   
$EndComp
Wire Wire Line
	2950 3450 3200 3450
$Comp
L Device:R R?
U 1 1 5F193DE8
P 2700 3050
AR Path="/5F193DE8" Ref="R?"  Part="1" 
AR Path="/5F142239/5F193DE8" Ref="R4"  Part="1" 
F 0 "R4" V 2700 2900 50  0000 C CNN
F 1 "1K" V 2700 3050 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2630 3050 50  0001 C CNN
F 3 "~" H 2700 3050 50  0001 C CNN
	1    2700 3050
	0    -1   -1   0   
$EndComp
Text GLabel 3200 3050 2    50   Input ~ 0
NMI*
Wire Wire Line
	3200 3250 3200 3050
Text GLabel 2400 5100 0    50   Input ~ 0
GND
$Comp
L Device:C_Small C?
U 1 1 5F193DF1
P 3100 5000
AR Path="/5F193DF1" Ref="C?"  Part="1" 
AR Path="/5F142239/5F193DF1" Ref="C20"  Part="1" 
F 0 "C20" V 2871 5000 50  0000 C CNN
F 1 "0.1uF" V 2962 5000 50  0000 C CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 3100 5000 50  0001 C CNN
F 3 "~" H 3100 5000 50  0001 C CNN
	1    3100 5000
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5F193DF7
P 2800 4900
AR Path="/5F193DF7" Ref="SW?"  Part="1" 
AR Path="/5F142239/5F193DF7" Ref="SW1"  Part="1" 
F 0 "SW1" H 2800 5050 50  0000 C CNN
F 1 "RESET" H 2800 4766 50  0000 C CNN
F 2 "Buttons_Switches_ThroughHole:SW_Tactile_SPST_Angled_PTS645Vx58-2LFS" H 2800 4900 50  0001 C CNN
F 3 "~" H 2800 4900 50  0001 C CNN
	1    2800 4900
	0    -1   1    0   
$EndComp
Text GLabel 3450 4700 2    50   Input ~ 0
RESET*
Wire Wire Line
	2850 3050 2950 3050
Text Notes 2500 5600 0    50   ~ 0
Bring APP* low during RESET* \nfor Macrostore Prototyping.\nRaise APP* after RESET* to\nenable Attached Processor.
Connection ~ 2950 3050
Wire Wire Line
	2950 3050 3200 3050
Text GLabel 2550 3050 0    50   Input ~ 0
+5V
Connection ~ 2800 4700
Wire Wire Line
	2700 4700 2800 4700
$Comp
L 74xx:74LS125 U?
U 1 1 5F193E05
P 3100 4150
AR Path="/5F193E05" Ref="U?"  Part="1" 
AR Path="/5F142239/5F193E05" Ref="U13"  Part="1" 
F 0 "U13" H 3100 4467 50  0000 C CNN
F 1 "74LS125" H 3100 4376 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3100 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 3100 4150 50  0001 C CNN
	1    3100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4700 3100 4700
Wire Wire Line
	3100 4400 3100 4700
Wire Wire Line
	2800 5100 3100 5100
Text GLabel 3450 4150 2    50   Input ~ 0
APP*
Wire Wire Line
	3400 4150 3450 4150
$Comp
L Device:R R?
U 1 1 5F193E10
P 2550 3800
AR Path="/5F193E10" Ref="R?"  Part="1" 
AR Path="/5F142239/5F193E10" Ref="R8"  Part="1" 
F 0 "R8" V 2550 3650 50  0000 C CNN
F 1 "10K" V 2550 3800 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2480 3800 50  0001 C CNN
F 3 "~" H 2550 3800 50  0001 C CNN
	1    2550 3800
	0    -1   -1   0   
$EndComp
Text GLabel 2400 3800 0    50   Input ~ 0
+5V
Connection ~ 3400 4150
Connection ~ 3100 4700
Wire Wire Line
	3100 4700 3450 4700
Wire Wire Line
	3100 4700 3100 4900
$Comp
L 74xx:74LS125 U?
U 5 1 5F193E1B
P 2400 4450
AR Path="/5F193E1B" Ref="U?"  Part="5" 
AR Path="/5F142239/5F193E1B" Ref="U13"  Part="5" 
F 0 "U13" H 2350 4500 50  0000 L CNN
F 1 "74LS125" H 2250 4400 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2400 4450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2400 4450 50  0001 C CNN
	5    2400 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F193E21
P 2550 3950
AR Path="/5F193E21" Ref="R?"  Part="1" 
AR Path="/5F142239/5F193E21" Ref="R1"  Part="1" 
F 0 "R1" V 2550 3800 50  0000 C CNN
F 1 "1K" V 2550 3950 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 2480 3950 50  0001 C CNN
F 3 "~" H 2550 3950 50  0001 C CNN
	1    2550 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2400 3800 2400 3950
Wire Wire Line
	2700 3800 3400 3800
Wire Wire Line
	3400 3800 3400 4150
Wire Wire Line
	2700 3950 2700 4700
Connection ~ 2400 3950
Wire Wire Line
	2400 4950 2400 5100
Wire Wire Line
	2400 5100 2800 5100
Connection ~ 2800 5100
Text GLabel 2800 4250 3    50   Input ~ 0
ZERO
Wire Wire Line
	2800 4150 2800 4250
$Comp
L 74xx:74LS125 U?
U 2 1 5F1A5229
P 2200 6250
AR Path="/5F1A5229" Ref="U?"  Part="1" 
AR Path="/5F142239/5F1A5229" Ref="U13"  Part="2" 
F 0 "U13" H 2200 6567 50  0000 C CNN
F 1 "74LS125" H 2200 6476 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2200 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2200 6250 50  0001 C CNN
	2    2200 6250
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS125 U?
U 3 1 5F1A8516
P 2950 6250
AR Path="/5F1A8516" Ref="U?"  Part="1" 
AR Path="/5F142239/5F1A8516" Ref="U13"  Part="3" 
F 0 "U13" H 2950 6567 50  0000 C CNN
F 1 "74LS125" H 2950 6476 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2950 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2950 6250 50  0001 C CNN
	3    2950 6250
	1    0    0    1   
$EndComp
$Comp
L 74xx:74LS125 U?
U 4 1 5F1A976D
P 3700 6250
AR Path="/5F1A976D" Ref="U?"  Part="1" 
AR Path="/5F142239/5F1A976D" Ref="U13"  Part="4" 
F 0 "U13" H 3700 6567 50  0000 C CNN
F 1 "74LS125" H 3700 6476 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3700 6250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 3700 6250 50  0001 C CNN
	4    3700 6250
	1    0    0    1   
$EndComp
Text Notes 2050 5800 0    50   ~ 0
Unused
Wire Wire Line
	1900 6250 1900 6500
Wire Wire Line
	3400 6250 3400 6500
Connection ~ 3400 6500
Wire Wire Line
	3400 6500 3700 6500
Wire Wire Line
	2650 6250 2650 6500
Connection ~ 2650 6500
NoConn ~ 2500 6250
NoConn ~ 3250 6250
NoConn ~ 4000 6250
Wire Wire Line
	1900 6500 2650 6500
Wire Wire Line
	2650 6500 3400 6500
Wire Wire Line
	1900 6000 2200 6000
Connection ~ 2200 6000
Wire Wire Line
	2200 6000 2950 6000
Connection ~ 2950 6000
Wire Wire Line
	2950 6000 3700 6000
Text GLabel 1900 6000 0    50   Input ~ 0
+5V
Wire Wire Line
	1900 6250 1900 6000
Connection ~ 1900 6250
Text Notes 3350 4300 0    50   ~ 0
TODO: APP* is not on the external bus
Wire Wire Line
	3200 1700 3200 1800
Wire Wire Line
	3200 2650 2400 2650
Wire Wire Line
	2400 2650 2400 2450
Connection ~ 3200 1800
Wire Wire Line
	3200 1800 3200 2350
Connection ~ 3200 2350
Wire Wire Line
	3200 2350 3200 2450
Connection ~ 3200 2450
Wire Wire Line
	3200 2450 3200 2650
Connection ~ 2400 1800
Wire Wire Line
	2400 1800 2400 1700
Connection ~ 2400 2350
Wire Wire Line
	2400 2350 2400 1800
Connection ~ 2400 2450
Wire Wire Line
	2400 2450 2400 2350
Text GLabel 2400 2650 0    50   Input ~ 0
GND
$EndSCHEMATC
