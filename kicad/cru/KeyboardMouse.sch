EESchema Schematic File Version 4
LIBS:Ruby-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:TMS9902 U?
U 1 1 5F2F346F
P 4000 2700
AR Path="/5EEDD9EE/5F2F346F" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F2F346F" Ref="U16"  Part="1" 
F 0 "U16" H 4000 3881 50  0000 C CNN
F 1 "TMS9902" H 4000 3790 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" H 4000 2700 50  0001 C CNN
F 3 "" H 4000 2700 50  0001 C CNN
	1    4000 2700
	1    0    0    -1  
$EndComp
Text GLabel 3200 2500 0    50   Input ~ 0
A0
Text GLabel 3200 2600 0    50   Input ~ 0
A1
Text GLabel 3200 2700 0    50   Input ~ 0
A2
Text GLabel 3200 2800 0    50   Input ~ 0
A3
Text GLabel 3200 2900 0    50   Input ~ 0
A4
Text GLabel 3200 3100 0    50   Input ~ 0
CLK3
Text GLabel 3200 3000 0    50   Input ~ 0
CRUCLK
Text GLabel 3200 3200 0    50   Input ~ 0
SEL_1440*
Text GLabel 3200 2400 0    50   Input ~ 0
CRUOUT
Text GLabel 3200 2300 0    50   Input ~ 0
CRUIN
Text Notes 4050 3050 0    50   ~ 0
RTS>
Text Notes 4100 3250 0    50   ~ 0
TX>
Text Notes 4050 2950 0    50   ~ 0
CTS<
$Comp
L Gemini:TMS9902 U?
U 1 1 5F2F3482
P 8050 2700
AR Path="/5EEDD9EE/5F2F3482" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F2F3482" Ref="U15"  Part="1" 
F 0 "U15" H 8050 3881 50  0000 C CNN
F 1 "TMS9902" H 8050 3790 50  0000 C CNN
F 2 "Housings_DIP:DIP-18_W7.62mm_Socket_LongPads" H 8050 2700 50  0001 C CNN
F 3 "" H 8050 2700 50  0001 C CNN
	1    8050 2700
	1    0    0    -1  
$EndComp
Text GLabel 7250 3100 0    50   Input ~ 0
CLK3
Text GLabel 7250 3200 0    50   Input ~ 0
SEL_1480*
Text GLabel 7250 2400 0    50   Input ~ 0
CRUOUT
Text GLabel 7250 2300 0    50   Input ~ 0
CRUIN
Text GLabel 7250 2200 0    50   Input ~ 0
INT_1480*
Text GLabel 6850 3900 0    50   Input ~ 0
+5V
Text GLabel 4000 1700 2    50   Input ~ 0
+5V
Text GLabel 4000 3700 0    50   Input ~ 0
GND
Text GLabel 8050 3700 0    50   Input ~ 0
GND
Text Label 4950 3200 2    50   ~ 0
XOUT4
Text Label 4700 3100 0    50   ~ 0
KBD_DATA_I
Text Label 4950 3000 2    50   ~ 0
RTS4
Text Label 4950 2900 2    50   ~ 0
CTS4
Text Label 9050 3200 2    50   ~ 0
XOUT5
Text Label 9050 3100 2    50   ~ 0
MUS_DATA_I
Text Label 9050 3000 2    50   ~ 0
RTS5
Text Label 9050 2900 2    50   ~ 0
CTS5
Text GLabel 7250 2500 0    50   Input ~ 0
A0
Text GLabel 7250 2600 0    50   Input ~ 0
A1
Text GLabel 7250 2700 0    50   Input ~ 0
A2
Text GLabel 7250 2800 0    50   Input ~ 0
A3
Text GLabel 7250 2900 0    50   Input ~ 0
A4
Text GLabel 3200 2200 0    50   Input ~ 0
INT_1440*
Text Notes 7000 1850 0    100  ~ 20
Mouse
Text Notes 4100 3150 0    50   ~ 0
RX<
Text Notes 2550 1650 0    100  ~ 20
Keyboard
Wire Wire Line
	4800 3100 5150 3100
Wire Wire Line
	5250 3000 5250 3400
Wire Wire Line
	4800 3000 5250 3000
Wire Wire Line
	4800 3200 5050 3200
Wire Wire Line
	4800 2800 5450 2800
NoConn ~ 5250 3400
Text Notes 4050 2850 0    50   ~ 0
DSR<
Text GLabel 7250 3000 0    50   Input ~ 0
CRUCLK
Text Label 9050 2800 2    50   ~ 0
MUS_CLK_I
Text Notes 8050 3050 0    50   ~ 0
RTS>
Text Notes 8100 3250 0    50   ~ 0
TX>
Text Notes 8050 2950 0    50   ~ 0
CTS<
Text Notes 8100 3150 0    50   ~ 0
RX<
Text Notes 8050 2850 0    50   ~ 0
DSR<
Wire Wire Line
	8850 2800 9450 2800
Wire Wire Line
	8850 3200 9050 3200
Wire Wire Line
	9150 3100 8850 3100
Wire Wire Line
	8850 3000 9250 3000
Wire Wire Line
	5050 3200 5050 3400
Text Label 3600 4400 0    50   ~ 0
B_KBD_CLK
Text Label 3600 4600 0    50   ~ 0
B_KBD_DATA
Text Label 7600 4400 0    50   ~ 0
B_MUS_CLK
Text Label 7600 4600 0    50   ~ 0
B_MUS_DATA
$Comp
L Device:R_Small R?
U 1 1 5F2F34E4
P 8200 4100
AR Path="/5EEDD9EE/5F2F34E4" Ref="R?"  Part="1" 
AR Path="/5F2EEAA0/5F2F34E4" Ref="R7"  Part="1" 
F 0 "R7" H 8150 4200 50  0000 L CNN
F 1 "3.3K" V 8100 4000 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 8200 4100 50  0001 C CNN
F 3 "~" H 8200 4100 50  0001 C CNN
	1    8200 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F2F3507
P 8550 4100
AR Path="/5EEDD9EE/5F2F3507" Ref="R?"  Part="1" 
AR Path="/5F2EEAA0/5F2F3507" Ref="R8"  Part="1" 
F 0 "R8" H 8500 4200 50  0000 L CNN
F 1 "3.3K" V 8450 4000 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 8550 4100 50  0001 C CNN
F 3 "~" H 8550 4100 50  0001 C CNN
	1    8550 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 4000 8200 3900
Wire Wire Line
	8850 2900 9700 2900
Text GLabel 1300 3900 0    50   Input ~ 0
+5V
$Comp
L 74xx:74LS125 U?
U 3 1 5F2F3525
P 5450 3600
AR Path="/5EEDD9EE/5F2F3525" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F2F3525" Ref="U9"  Part="3" 
F 0 "U9" V 5496 3780 50  0000 L CNN
F 1 "74LS125" V 5405 3780 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5450 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 5450 3600 50  0001 C CNN
	3    5450 3600
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74LS125 U?
U 2 1 5F2F352B
P 5150 4000
AR Path="/5EEDD9EE/5F2F352B" Ref="U?"  Part="2" 
AR Path="/5F2EEAA0/5F2F352B" Ref="U9"  Part="2" 
F 0 "U9" V 5196 4180 50  0000 L CNN
F 1 "74LS125" V 5105 4180 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5150 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 5150 4000 50  0001 C CNN
	2    5150 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F2F3540
P 4200 4100
AR Path="/5EEDD9EE/5F2F3540" Ref="R?"  Part="1" 
AR Path="/5F2EEAA0/5F2F3540" Ref="R3"  Part="1" 
F 0 "R3" H 4150 4200 50  0000 L CNN
F 1 "3.3K" V 4100 4000 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 4200 4100 50  0001 C CNN
F 3 "~" H 4200 4100 50  0001 C CNN
	1    4200 4100
	1    0    0    -1  
$EndComp
Text GLabel 1350 5300 0    50   Input ~ 0
GND
Wire Wire Line
	3600 4600 4200 4600
Connection ~ 4200 4600
Wire Wire Line
	4200 4600 5150 4600
Wire Wire Line
	4200 4200 4200 4600
$Comp
L Device:R_Small R?
U 1 1 5F2F3562
P 4500 4100
AR Path="/5EEDD9EE/5F2F3562" Ref="R?"  Part="1" 
AR Path="/5F2EEAA0/5F2F3562" Ref="R6"  Part="1" 
F 0 "R6" H 4450 4200 50  0000 L CNN
F 1 "3.3K" V 4400 4000 50  0000 L CNN
F 2 "Resistors_SMD:R_1206" H 4500 4100 50  0001 C CNN
F 3 "~" H 4500 4100 50  0001 C CNN
	1    4500 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4000 4200 3900
Wire Wire Line
	5700 2900 5700 3600
Wire Wire Line
	4800 2900 5700 2900
NoConn ~ 5050 3400
Text GLabel 6350 3000 1    50   Input ~ 0
KEY_CLK_O
Text GLabel 5950 3000 1    50   Input ~ 0
KEY_DATA_O
Text GLabel 10350 3000 1    50   Input ~ 0
MOUSE_CLK_O
Text GLabel 9950 3000 1    50   Input ~ 0
MOUSE_DATA_O
Text GLabel 8050 1700 0    50   Input ~ 0
+5V
$Comp
L 74xx:74LS125 U?
U 5 1 5F32AE92
P 1500 4600
AR Path="/5EEDD9EE/5F32AE92" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F32AE92" Ref="U9"  Part="5" 
F 0 "U9" H 1730 4646 50  0000 L CNN
F 1 "74LS125" H 1730 4555 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 1500 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 1500 4600 50  0001 C CNN
	5    1500 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 5300 1500 5300
Wire Wire Line
	5450 2800 5450 3300
$Comp
L 74xx:74LS125 U?
U 1 1 5F3C5491
P 5950 4000
AR Path="/5EEDD9EE/5F3C5491" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F3C5491" Ref="U9"  Part="1" 
F 0 "U9" V 5996 4180 50  0000 L CNN
F 1 "74LS125" V 5905 4180 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 5950 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 5950 4000 50  0001 C CNN
	1    5950 4000
	0    -1   1    0   
$EndComp
$Comp
L 74xx:74LS125 U?
U 4 1 5F3C84F2
P 6350 3600
AR Path="/5EEDD9EE/5F3C84F2" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F3C84F2" Ref="U9"  Part="4" 
F 0 "U9" V 6396 3780 50  0000 L CNN
F 1 "74LS125" V 6305 3780 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 6350 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 6350 3600 50  0001 C CNN
	4    6350 3600
	0    -1   1    0   
$EndComp
Connection ~ 5700 3600
Wire Wire Line
	5150 3100 5150 3700
Wire Wire Line
	5700 3600 5700 4000
Wire Wire Line
	5400 4000 5700 4000
Wire Wire Line
	3600 4400 4500 4400
Wire Wire Line
	4200 3900 4500 3900
Wire Wire Line
	4500 3900 4500 4000
Connection ~ 4200 3900
Wire Wire Line
	4500 4200 4500 4400
Connection ~ 4500 4400
Wire Wire Line
	4500 4400 5450 4400
Connection ~ 5150 4600
Wire Wire Line
	5150 4300 5150 4600
$Comp
L 74xx:74LS125 U?
U 5 1 5F42D5BE
P 2150 4600
AR Path="/5EEDD9EE/5F42D5BE" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F42D5BE" Ref="U11"  Part="5" 
F 0 "U11" H 2380 4646 50  0000 L CNN
F 1 "74LS125" H 2380 4555 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 2150 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 2150 4600 50  0001 C CNN
	5    2150 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4300 5950 4600
Wire Wire Line
	6350 4400 5450 4400
Wire Wire Line
	6350 3900 6350 4400
Connection ~ 5450 4400
Wire Wire Line
	5950 3000 5950 3700
Wire Wire Line
	6600 3600 6600 3300
Wire Wire Line
	6200 4000 6200 3700
Wire Wire Line
	6200 3700 5950 3700
Connection ~ 5950 3700
Wire Wire Line
	6350 3300 6600 3300
Connection ~ 6350 3300
Wire Wire Line
	6350 3000 6350 3300
Wire Wire Line
	9250 3000 9250 3400
NoConn ~ 9250 3400
Wire Wire Line
	9050 3200 9050 3400
$Comp
L 74xx:74LS125 U?
U 2 1 5F456B07
P 9450 3600
AR Path="/5EEDD9EE/5F456B07" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F456B07" Ref="U11"  Part="2" 
F 0 "U11" V 9496 3780 50  0000 L CNN
F 1 "74LS125" V 9405 3780 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9450 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9450 3600 50  0001 C CNN
	2    9450 3600
	0    -1   -1   0   
$EndComp
$Comp
L 74xx:74LS125 U?
U 3 1 5F456B0D
P 9150 4000
AR Path="/5EEDD9EE/5F456B0D" Ref="U?"  Part="2" 
AR Path="/5F2EEAA0/5F456B0D" Ref="U11"  Part="3" 
F 0 "U11" V 9196 4180 50  0000 L CNN
F 1 "74LS125" V 9105 4180 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9150 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9150 4000 50  0001 C CNN
	3    9150 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9700 2900 9700 3600
NoConn ~ 9050 3400
$Comp
L 74xx:74LS125 U?
U 4 1 5F456B16
P 9950 4000
AR Path="/5EEDD9EE/5F456B16" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F456B16" Ref="U11"  Part="4" 
F 0 "U11" V 9996 4180 50  0000 L CNN
F 1 "74LS125" V 9905 4180 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9950 4000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 9950 4000 50  0001 C CNN
	4    9950 4000
	0    -1   1    0   
$EndComp
$Comp
L 74xx:74LS125 U?
U 1 1 5F456B1C
P 10350 3600
AR Path="/5EEDD9EE/5F456B1C" Ref="U?"  Part="1" 
AR Path="/5F2EEAA0/5F456B1C" Ref="U11"  Part="1" 
F 0 "U11" V 10396 3780 50  0000 L CNN
F 1 "74LS125" V 10305 3780 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 10350 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS125" H 10350 3600 50  0001 C CNN
	1    10350 3600
	0    -1   1    0   
$EndComp
Connection ~ 9700 3600
Wire Wire Line
	9150 3100 9150 3700
Wire Wire Line
	9700 3600 9700 4000
Wire Wire Line
	9400 4000 9700 4000
Connection ~ 9150 4600
Wire Wire Line
	9150 4300 9150 4600
Wire Wire Line
	9950 4300 9950 4600
Wire Wire Line
	9150 4600 9950 4600
Wire Wire Line
	10350 4400 9450 4400
Wire Wire Line
	10350 3900 10350 4400
Connection ~ 9450 4400
Wire Wire Line
	9950 3000 9950 3700
Wire Wire Line
	10200 4000 10200 3700
Wire Wire Line
	10200 3700 9950 3700
Connection ~ 9950 3700
Wire Wire Line
	9450 3900 9450 4400
Wire Wire Line
	5450 3900 5450 4400
Wire Wire Line
	10350 3000 10350 3300
Wire Wire Line
	10600 3600 10600 3300
Wire Wire Line
	10600 3300 10350 3300
Connection ~ 10350 3300
Wire Wire Line
	9450 3300 9450 2800
Wire Wire Line
	9700 4000 9700 5300
Connection ~ 9700 4000
Wire Wire Line
	7600 4400 8550 4400
Wire Wire Line
	7600 4600 8200 4600
Connection ~ 5700 4000
Wire Wire Line
	5150 4600 5700 4600
Wire Wire Line
	5700 4600 5950 4600
Connection ~ 5700 5300
Wire Wire Line
	5700 4000 5700 5300
Wire Wire Line
	8550 4200 8550 4400
Connection ~ 8550 4400
Wire Wire Line
	8550 4400 9450 4400
Wire Wire Line
	8200 4200 8200 4600
Connection ~ 8200 4600
Wire Wire Line
	8200 4600 9150 4600
Wire Wire Line
	8200 3900 8550 3900
Wire Wire Line
	8550 3900 8550 4000
Connection ~ 8200 3900
Text Label 4800 2800 0    50   ~ 0
KBD_CLK_I
Wire Wire Line
	1500 5100 1500 5300
Connection ~ 1500 5300
Wire Wire Line
	1300 3900 1500 3900
Wire Wire Line
	1500 4100 1500 3900
Connection ~ 1500 3900
Wire Wire Line
	1500 3900 2150 3900
Wire Wire Line
	2150 4100 2150 3900
Connection ~ 2150 3900
Text GLabel 7600 4400 0    50   Input ~ 0
B_MUS_CLK
Text GLabel 7600 4600 0    50   Input ~ 0
B_MUS_DATA
Wire Wire Line
	5700 5300 9700 5300
Text GLabel 3600 4400 0    50   Input ~ 0
B_KBD_CLK
Text GLabel 3600 4600 0    50   Input ~ 0
B_KBD_DATA
Wire Wire Line
	2150 3900 4200 3900
Wire Wire Line
	6850 3900 8200 3900
Wire Wire Line
	2150 5300 5700 5300
Wire Wire Line
	1500 5300 2150 5300
Connection ~ 2150 5300
Wire Wire Line
	2150 5100 2150 5300
$EndSCHEMATC
