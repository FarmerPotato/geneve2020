EESchema Schematic File Version 4
LIBS:power-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Gemini:ATX_20_PS ATX1
U 1 1 601475CE
P 9850 3050
F 0 "ATX1" H 9850 3865 50  0000 C CNN
F 1 "ATX_20_PS" H 9850 3774 50  0000 C CNN
F 2 "Connectors_Molex:Molex_MiniFit-JR-5556-20B_2x10x4.20mm_Straight" H 9850 3550 50  0001 C CNN
F 3 "" H 9850 3550 50  0001 C CNN
	1    9850 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60147B77
P 9850 4050
F 0 "#PWR04" H 9850 3800 50  0001 C CNN
F 1 "GND" H 9855 3877 50  0000 C CNN
F 2 "" H 9850 4050 50  0001 C CNN
F 3 "" H 9850 4050 50  0001 C CNN
	1    9850 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 60147CB0
P 8150 2900
F 0 "#PWR02" H 8150 2750 50  0001 C CNN
F 1 "+5V" H 8165 3073 50  0000 C CNN
F 2 "" H 8150 2900 50  0001 C CNN
F 3 "" H 8150 2900 50  0001 C CNN
	1    8150 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR03
U 1 1 6014860A
P 8150 3500
F 0 "#PWR03" H 8150 3350 50  0001 C CNN
F 1 "+12V" H 8165 3673 50  0000 C CNN
F 2 "" H 8150 3500 50  0001 C CNN
F 3 "" H 8150 3500 50  0001 C CNN
	1    8150 3500
	1    0    0    -1  
$EndComp
NoConn ~ 10600 3300
NoConn ~ 10600 2700
NoConn ~ 9100 3400
Wire Wire Line
	10600 2800 10800 2800
NoConn ~ 9100 3100
NoConn ~ 9100 2700
NoConn ~ 10600 2600
NoConn ~ 10600 3400
NoConn ~ 10600 3500
Connection ~ 9850 4050
Wire Wire Line
	9850 4050 10800 4050
$Comp
L Switch:SW_DPST SW1
U 1 1 6014FB04
P 9800 1850
F 0 "SW1" H 9800 2175 50  0000 C CNN
F 1 "SW_DPST" H 9800 2084 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 9800 1850 50  0001 C CNN
F 3 "~" H 9800 1850 50  0001 C CNN
	1    9800 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 2900 10900 2900
Wire Wire Line
	9600 1950 9600 2050
Wire Wire Line
	9600 1750 9350 1750
$Comp
L Device:R R1
U 1 1 60152421
P 9200 1750
F 0 "R1" V 8993 1750 50  0000 C CNN
F 1 "R" V 9084 1750 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9130 1750 50  0001 C CNN
F 3 "~" H 9200 1750 50  0001 C CNN
	1    9200 1750
	0    1    1    0   
$EndComp
Connection ~ 10800 2800
Wire Wire Line
	10900 2900 10900 1950
Text Label 10300 1950 2    50   ~ 0
PS_ON*
$Comp
L Motor:Fan_IEC60617 F1
U 1 1 6015A389
P 7750 3750
F 0 "F1" H 7590 3754 50  0000 R CNN
F 1 "Fan_12V" H 7590 3845 50  0000 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 7800 3610 50  0001 L CNN
F 3 "~" H 7750 3760 50  0001 C CNN
	1    7750 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 60163E89
P 6600 4350
F 0 "J4" V 6708 4062 50  0000 R CNN
F 1 "Conn_01x03_Male" V 6663 4062 50  0001 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 6600 4350 50  0001 C CNN
F 3 "~" H 6600 4350 50  0001 C CNN
	1    6600 4350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 601649C0
P 5950 4350
F 0 "J3" V 6058 4062 50  0000 R CNN
F 1 "Conn_01x03_Male" V 6013 4062 50  0001 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 5950 4350 50  0001 C CNN
F 3 "~" H 5950 4350 50  0001 C CNN
	1    5950 4350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 6016553F
P 5300 4350
F 0 "J2" V 5408 4062 50  0000 R CNN
F 1 "Conn_01x03_Male" V 5363 4062 50  0001 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 5300 4350 50  0001 C CNN
F 3 "~" H 5300 4350 50  0001 C CNN
	1    5300 4350
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 601696F4
P 4650 4350
F 0 "J1" V 4758 4062 50  0000 R CNN
F 1 "Conn_01x03_Male" V 4713 4062 50  0001 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 4650 4350 50  0001 C CNN
F 3 "~" H 4650 4350 50  0001 C CNN
	1    4650 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 4150 4550 4050
Wire Wire Line
	4550 4050 5200 4050
Wire Wire Line
	6500 4050 6500 4150
Connection ~ 6500 4050
Wire Wire Line
	6500 4050 5850 4050
Wire Wire Line
	5850 4050 5850 4150
Connection ~ 5850 4050
Wire Wire Line
	5850 4050 5200 4050
Wire Wire Line
	5200 4050 5200 4150
Connection ~ 5200 4050
$Comp
L power:+3V3 #PWR01
U 1 1 60147F2D
P 8150 2600
F 0 "#PWR01" H 8150 2450 50  0001 C CNN
F 1 "+3V3" V 8165 2728 50  0000 L CNN
F 2 "" H 8150 2600 50  0001 C CNN
F 3 "" H 8150 2600 50  0001 C CNN
	1    8150 2600
	1    0    0    -1  
$EndComp
Connection ~ 8150 2900
Connection ~ 8150 2600
Connection ~ 8150 3500
Text Label 5050 2900 2    50   ~ 0
+5V
Text Label 5100 2600 2    50   ~ 0
+3V3
Text Label 5050 4050 2    50   ~ 0
GND
Wire Wire Line
	7750 3550 7750 3500
Connection ~ 7750 3500
Wire Wire Line
	7750 3500 8150 3500
Wire Wire Line
	7350 3550 7350 3500
Wire Wire Line
	7350 3500 7750 3500
Wire Wire Line
	8150 3500 8400 3500
Wire Wire Line
	8150 2900 8800 2900
Wire Wire Line
	8150 2600 9100 2600
Wire Wire Line
	10800 2800 10800 3000
Wire Wire Line
	10800 2050 10800 1750
Wire Wire Line
	10800 2050 10800 2800
Connection ~ 10800 2050
Wire Wire Line
	9600 2050 10800 2050
Wire Wire Line
	10000 1950 10900 1950
Wire Wire Line
	6500 4050 7350 4050
Connection ~ 7750 4050
Connection ~ 7350 4050
Wire Wire Line
	7350 4050 7750 4050
Text Notes 8400 4250 2    50   ~ 0
Until these are keyed headers,\npin 1 is ground
$Comp
L Mechanical:MountingHole H1
U 1 1 601D7711
P 10200 5550
F 0 "H1" H 10300 5596 50  0000 L CNN
F 1 "MountingHole" H 10300 5505 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 10200 5550 50  0001 C CNN
F 3 "~" H 10200 5550 50  0001 C CNN
	1    10200 5550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 601DE17E
P 10200 5800
F 0 "H2" H 10300 5846 50  0000 L CNN
F 1 "MountingHole" H 10300 5755 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 10200 5800 50  0001 C CNN
F 3 "~" H 10200 5800 50  0001 C CNN
	1    10200 5800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 601DEE17
P 10200 6050
F 0 "H3" H 10300 6096 50  0000 L CNN
F 1 "MountingHole" H 10300 6005 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 10200 6050 50  0001 C CNN
F 3 "~" H 10200 6050 50  0001 C CNN
	1    10200 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 1750 8800 1750
Wire Wire Line
	10000 1750 10800 1750
Text Label 9500 1750 2    50   ~ 0
LED
$Comp
L Gemini:DIN-5 J7
U 1 1 5FAE79B8
P 3150 3150
F 0 "J7" H 3150 2875 50  0000 C CNN
F 1 "DIN-5" H 3150 2784 50  0000 C CNN
F 2 "Gemini:DIN-5_180_1cm" H 3150 3150 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 3150 3150 50  0001 C CNN
	1    3150 3150
	1    0    0    -1  
$EndComp
$Comp
L Gemini:DIN-5 J6
U 1 1 5FAE7F0F
P 3150 2300
F 0 "J6" H 3150 2025 50  0000 C CNN
F 1 "DIN-5" H 3150 1934 50  0000 C CNN
F 2 "Gemini:DIN-5_180_1cm" H 3150 2300 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 3150 2300 50  0001 C CNN
	1    3150 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5FAF5ACD
P 2650 3500
F 0 "C1" H 2765 3546 50  0000 L CNN
F 1 ".1uF" H 2765 3455 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 2688 3350 50  0001 C CNN
F 3 "~" H 2650 3500 50  0001 C CNN
	1    2650 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5FB01D8F
P 2450 2650
F 0 "C2" H 2565 2696 50  0000 L CNN
F 1 ".1uF" H 2565 2605 50  0000 L CNN
F 2 "Gemini:C_Disc_D3.8mm_W2.6mm_P4.00mm" H 2488 2500 50  0001 C CNN
F 3 "~" H 2450 2650 50  0001 C CNN
	1    2450 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3050 2850 3050
Wire Wire Line
	2650 3050 2650 3350
NoConn ~ 2850 2400
NoConn ~ 3150 2000
NoConn ~ 3150 2850
NoConn ~ 2850 3250
Wire Wire Line
	4550 4050 3600 4050
Connection ~ 4550 4050
Wire Wire Line
	3600 4150 3600 4050
Connection ~ 3600 4050
Wire Wire Line
	3700 2900 3700 4150
Wire Wire Line
	2650 3650 2650 4050
Connection ~ 2650 4050
Wire Wire Line
	2650 4050 3600 4050
Text Label 4000 3050 3    50   ~ 0
MIDI_IN_P4
Text Label 4100 3050 3    50   ~ 0
MIDI_IN_P5
Text Label 3900 3950 1    50   ~ 0
MIDI_OUT_P5
Text Label 3800 3950 1    50   ~ 0
MIDI_OUT_P4
Wire Wire Line
	3450 2400 4100 2400
Wire Wire Line
	4100 2400 4100 4150
Wire Wire Line
	4000 4150 4000 2200
Wire Wire Line
	3450 2200 4000 2200
Wire Wire Line
	3900 4150 3900 3250
Wire Wire Line
	3900 3250 3450 3250
Wire Wire Line
	3450 3050 3800 3050
Wire Wire Line
	3800 3050 3800 4150
Wire Wire Line
	2450 2200 2450 2500
Wire Wire Line
	2450 2200 2850 2200
$Comp
L Connector:Conn_01x08_Male J8
U 1 1 5FB43519
P 3900 4350
F 0 "J8" V 3827 4278 50  0000 C CNN
F 1 "Conn_01x08_Male" V 3963 3962 50  0001 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 3900 4350 50  0001 C CNN
F 3 "~" H 3900 4350 50  0001 C CNN
	1    3900 4350
	0    -1   -1   0   
$EndComp
$Comp
L Gemini:PS2_Keyboard_Mouse_Stacked P1
U 1 1 5FAE8605
P 1600 3450
F 0 "P1" H 1494 4426 50  0000 C CNN
F 1 "PS2_Keyboard_Mouse_Stacked" H 1494 4335 50  0000 C CNN
F 2 "Gemini:PS2_Keyboard_Mouse_Stacked" H 1600 3250 50  0001 C CNN
F 3 "" H 1600 3470 50  0000 C CNN
	1    1600 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 2800 2450 2950
Wire Wire Line
	2450 4050 2650 4050
Wire Wire Line
	2000 3550 2000 4150
Wire Wire Line
	1900 3350 2100 3350
Wire Wire Line
	2100 3350 2100 4150
Wire Wire Line
	1900 3050 2200 3050
Wire Wire Line
	2200 3050 2200 4150
Text GLabel 1050 2950 0    50   Input ~ 0
+5V
Text GLabel 1050 3450 0    50   Input ~ 0
+5V
Wire Wire Line
	1050 3450 1300 3450
Wire Wire Line
	1050 2950 1300 2950
NoConn ~ 1300 2850
NoConn ~ 1300 3050
NoConn ~ 1300 3350
NoConn ~ 1300 3550
Wire Wire Line
	1900 2850 2300 2850
Wire Wire Line
	2300 2850 2300 4150
Text Label 2300 3650 3    50   ~ 0
MUS_CLK
Text Label 2200 4000 1    50   ~ 0
MUS_DATA
Text Label 2100 4000 1    50   ~ 0
KBD_CLK
Text Label 2000 4000 1    50   ~ 0
KBD_DATA
Wire Wire Line
	1900 3550 2000 3550
Wire Wire Line
	4200 4800 4200 4150
Wire Wire Line
	4300 4150 4300 4900
Text Label 3500 4800 2    50   ~ 0
MIDI_OUT
Text Label 3500 4900 2    50   ~ 0
MIDI_IN
Text GLabel 7950 2900 1    50   Input ~ 0
+5V
$Comp
L Connector:Conn_01x06_Male J9
U 1 1 5FBF4A75
P 2000 4350
F 0 "J9" V 2154 3962 50  0000 R CNN
F 1 "Conn_01x06_Male" V 2063 3962 50  0000 R CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 2000 4350 50  0001 C CNN
F 3 "~" H 2000 4350 50  0001 C CNN
	1    2000 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4300 4900 1900 4900
Wire Wire Line
	1800 4150 1800 4800
Wire Wire Line
	1800 4800 4200 4800
Wire Wire Line
	1900 4150 1900 4900
Text Notes 3750 5050 2    50   ~ 0
connections to 9902
$Comp
L Gemini:AudioJack3_Combo_SwitchPD J5
U 1 1 5FC2F778
P 1350 7400
F 0 "J5" H 1382 7875 50  0000 C CNN
F 1 "AudioJack3_Combo_SwitchPD" H 1382 7784 50  0000 C CNN
F 2 "Gemini:Phono_3_Stacked" H 1200 7650 50  0001 C CNN
F 3 "~" H 1200 7650 50  0001 C CNN
	1    1350 7400
	1    0    0    -1  
$EndComp
$Comp
L Gemini:AudioJack3_Combo_SwitchPD J5
U 2 1 5FC35694
P 1350 6450
F 0 "J5" H 1382 6925 50  0000 C CNN
F 1 "AudioJack3_Combo_SwitchPD" H 1382 6834 50  0000 C CNN
F 2 "Gemini:Phono_3_Stacked" H 1200 6700 50  0001 C CNN
F 3 "~" H 1200 6700 50  0001 C CNN
	2    1350 6450
	1    0    0    -1  
$EndComp
$Comp
L Gemini:AudioJack3_Combo_SwitchPD J5
U 3 1 5FC45703
P 1350 5500
F 0 "J5" H 1382 5975 50  0000 C CNN
F 1 "AudioJack3_Combo_SwitchPD" H 1382 5884 50  0000 C CNN
F 2 "Gemini:Phono_3_Stacked" H 1200 5750 50  0001 C CNN
F 3 "~" H 1200 5750 50  0001 C CNN
	3    1350 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6200 2000 5250
Wire Wire Line
	2000 5250 1600 5250
Connection ~ 2000 6200
Wire Wire Line
	2000 6200 1600 6200
Wire Wire Line
	2000 7150 1600 7150
Wire Wire Line
	2000 6200 2000 7150
Text GLabel 1600 7450 2    50   Input ~ 0
+5V
Wire Wire Line
	1600 6500 1600 6400
Text Label 2000 6200 2    50   ~ 0
GND
Wire Wire Line
	1700 6500 1700 6600
Wire Wire Line
	1700 6600 1600 6600
Wire Wire Line
	1600 6750 1800 6750
Wire Wire Line
	1800 6750 1800 6600
Text Label 2050 6400 0    50   ~ 0
LINE_OUT_R
Text Label 2050 6300 0    50   ~ 0
LINE_OUT_L
Text Label 2100 5350 0    50   ~ 0
LINE_IN_L
Text Label 2100 5550 0    50   ~ 0
LINE_IN_R
Text Label 2050 6500 0    50   ~ 0
LINE_OUT_PD1
Text Label 2050 6600 0    50   ~ 0
LINE_OUT_PD2
Text Label 2100 5650 0    50   ~ 0
LINE_IN_PD1
Text Label 2100 5800 0    50   ~ 0
LINE_IN_PD2
Wire Wire Line
	2100 7250 1600 7250
Text Label 2400 6700 2    50   ~ 0
MIC
Text Label 2300 7550 0    50   ~ 0
MIC_PD1
Text Label 2300 7700 0    50   ~ 0
MIC_PD2
$Comp
L Mechanical:MountingHole H4
U 1 1 601DEE21
P 10200 6300
F 0 "H4" H 10300 6346 50  0000 L CNN
F 1 "MountingHole" H 10300 6255 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 10200 6300 50  0001 C CNN
F 3 "~" H 10200 6300 50  0001 C CNN
	1    10200 6300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x12 J10
U 1 1 5FD062E8
P 4400 7300
F 0 "J10" V 4525 7246 50  0000 C CNN
F 1 "Conn_01x12" V 4616 7246 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06_Pitch2.54mm" H 4400 7300 50  0001 C CNN
F 3 "~" H 4400 7300 50  0001 C CNN
	1    4400 7300
	0    1    1    0   
$EndComp
Text Label 4900 7100 1    50   ~ 0
GND
Text Label 4800 7100 1    50   ~ 0
MIC
Text Label 4400 7100 1    50   ~ 0
LINE_IN_R
Text Label 4000 7100 1    50   ~ 0
LINE_IN_PD1
Text Label 4200 7100 1    50   ~ 0
LINE_IN_PD2
Text Label 3800 7100 1    50   ~ 0
LINE_OUT_PD1
Text Label 4600 7100 1    50   ~ 0
MIC_PD2
Text Label 4700 7100 1    50   ~ 0
MIC_PD1
Text Label 4500 7100 1    50   ~ 0
LINE_IN_L
Text Label 4300 7100 1    50   ~ 0
LINE_OUT_L
Text Label 4100 7100 1    50   ~ 0
LINE_OUT_PD2
Text Label 3900 7100 1    50   ~ 0
LINE_OUT_R
Wire Wire Line
	1600 7550 4700 7550
Wire Wire Line
	2100 7450 2100 7250
Wire Wire Line
	4600 7700 4600 7100
Wire Wire Line
	1600 7700 4600 7700
Wire Wire Line
	4800 7450 4800 7100
Wire Wire Line
	2100 7450 4800 7450
Wire Wire Line
	4700 7550 4700 7100
Wire Wire Line
	4500 7100 4500 5350
Wire Wire Line
	1600 5350 4500 5350
Wire Wire Line
	4400 7100 4400 5550
Wire Wire Line
	1600 5550 4400 5550
Wire Wire Line
	4000 5650 4000 7100
Wire Wire Line
	1600 5650 4000 5650
Wire Wire Line
	4200 5800 4200 7100
Wire Wire Line
	1600 5800 4200 5800
Wire Wire Line
	4900 6200 4900 7100
Wire Wire Line
	2000 6200 4900 6200
Wire Wire Line
	3900 7100 3900 6400
Wire Wire Line
	1600 6400 3900 6400
Wire Wire Line
	4300 6300 4300 7100
Wire Wire Line
	1600 6300 4300 6300
Wire Wire Line
	3800 6500 3800 7100
Wire Wire Line
	1700 6500 3800 6500
Wire Wire Line
	4100 6600 4100 7100
Wire Wire Line
	1800 6600 4100 6600
Wire Wire Line
	10600 3000 10800 3000
Connection ~ 10800 3000
Wire Wire Line
	10800 3000 10800 3100
Wire Wire Line
	10600 3100 10800 3100
Connection ~ 10800 3100
Wire Wire Line
	10800 3100 10800 3200
Wire Wire Line
	10600 3200 10800 3200
Connection ~ 10800 3200
Wire Wire Line
	10800 3200 10800 4050
Wire Wire Line
	7750 4050 8400 4050
Wire Wire Line
	8900 3200 9100 3200
Wire Wire Line
	8900 3200 8900 4050
Connection ~ 8900 4050
Wire Wire Line
	8900 4050 9850 4050
Wire Wire Line
	8900 3200 8900 3000
Wire Wire Line
	8900 3000 9100 3000
Connection ~ 8900 3200
Wire Wire Line
	8900 3000 8900 2800
Wire Wire Line
	8900 2800 9100 2800
Connection ~ 8900 3000
Wire Wire Line
	8800 1750 8800 2900
Connection ~ 8800 2900
Wire Wire Line
	8800 2900 9100 2900
$Comp
L Device:R R2
U 1 1 5FE17D20
P 8400 3800
F 0 "R2" V 8193 3800 50  0000 C CNN
F 1 "R" V 8284 3800 50  0000 C CNN
F 2 "Resistors_ThroughHole:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8330 3800 50  0001 C CNN
F 3 "~" H 8400 3800 50  0001 C CNN
	1    8400 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 3650 8400 3500
Connection ~ 8400 3500
Wire Wire Line
	8400 3500 9100 3500
Wire Wire Line
	8400 3950 8400 4050
Connection ~ 8400 4050
Wire Wire Line
	8400 4050 8650 4050
$Comp
L Motor:Fan_IEC60617 F2
U 1 1 60159920
P 7350 3750
F 0 "F2" H 7190 3754 50  0000 R CNN
F 1 "Fan_12V" H 7190 3845 50  0000 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-02_02x2.54mm_Straight" H 7400 3610 50  0001 L CNN
F 3 "~" H 7350 3760 50  0001 C CNN
	1    7350 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 4150 6600 2900
Connection ~ 6600 2900
Wire Wire Line
	6600 2900 7000 2900
Wire Wire Line
	6700 4150 6700 2600
Connection ~ 6700 2600
Wire Wire Line
	6700 2600 8150 2600
Wire Wire Line
	5950 4150 5950 2900
Connection ~ 5950 2900
Wire Wire Line
	5950 2900 6600 2900
Wire Wire Line
	6050 4150 6050 2600
Connection ~ 6050 2600
Wire Wire Line
	6050 2600 6700 2600
Wire Wire Line
	5300 4150 5300 2900
Connection ~ 5300 2900
Wire Wire Line
	5300 2900 5950 2900
Wire Wire Line
	5400 4150 5400 2600
Connection ~ 5400 2600
Wire Wire Line
	5400 2600 6050 2600
Wire Wire Line
	4650 4150 4650 2900
Wire Wire Line
	3700 2900 4650 2900
Connection ~ 4650 2900
Wire Wire Line
	4650 2900 5300 2900
Wire Wire Line
	4750 4150 4750 2600
Wire Wire Line
	4750 2600 5400 2600
$Comp
L Connector:Conn_01x02_Male J11
U 1 1 5FEAD9EB
P 6900 4350
F 0 "J11" V 7054 4162 50  0000 R CNN
F 1 "Conn_01x02_Male" V 6963 4162 50  0000 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-02_02x2.54mm_Straight" H 6900 4350 50  0001 C CNN
F 3 "~" H 6900 4350 50  0001 C CNN
	1    6900 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 4150 6900 3300
Wire Wire Line
	6900 3300 9100 3300
Wire Wire Line
	7000 4150 7000 2900
Connection ~ 7000 2900
Wire Wire Line
	7000 2900 8150 2900
$Comp
L Connector:Conn_01x02_Male J12
U 1 1 5FEBF50B
P 8650 4350
F 0 "J12" V 8804 4162 50  0000 R CNN
F 1 "Conn_01x02_Male" V 8713 4162 50  0000 R CNN
F 2 "Connectors_Molex:Molex_KK-6410-02_02x2.54mm_Straight" H 8650 4350 50  0001 C CNN
F 3 "~" H 8650 4350 50  0001 C CNN
	1    8650 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9100 3400 8750 3400
Wire Wire Line
	8750 3400 8750 4150
Wire Wire Line
	8650 4150 8650 4050
Connection ~ 8650 4050
Wire Wire Line
	8650 4050 8900 4050
Wire Wire Line
	1900 2950 2450 2950
Connection ~ 2450 2950
Wire Wire Line
	2450 2950 2450 3450
Wire Wire Line
	1900 3450 2450 3450
Connection ~ 2450 3450
Wire Wire Line
	2450 3450 2450 4050
Text Label 6900 3700 3    50   ~ 0
PWR_GD
Text Label 8750 3950 1    50   ~ 0
+5VSB
$EndSCHEMATC
