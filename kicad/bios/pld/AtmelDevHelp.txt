WinCupl included help files.

Here are the G22V10 chips. I think it is saying that the 22V10C/CZ
can be used without power down mode by programming it as G22V10LCC.
Yes, this is repeated in ReleaseNotes under ATF22V10C/CZ.

If you are generating a sim file, You must set pin 5 to 0. See CUPL_BUG.TXT
No sign of this in CUPL_BUG.txt or CUPL_FIX.txt now. This is version 5.30.4. 


%%
%G22V10cp
G22V10 Architecture
Mnemonic: G22V10 PLCC Mnemonic: YES (G22V10CPLCC)
DIP Pin Count: 24 Total Product Terms: 132
Extensions: OE D AR SP Pin Controlled Power Down: Yes
__________________________________________________________
Manufacturer Device Name
------------ -----------
ATMEL ATF22V10C/CZ
Clock Pin(s): 1 Common OE(s):
VCC(s): 24 GND(s): 12
Input Only: 1 2 3 4 5 6 7 8 9 10 11 13
Output Only:
Input/Output: 14 15 16 17 18 19 20 21 22 23
____________________________________________________________
Device Notes:
A. Pin Controlled Power Down
Select this device type if you want to use the pin-controlled
power down feature enabled. When Pin 4 is set to a logic high
(5V) the device will automatically power down to a standy
current of (100uA) for the ATF22V10C. This feature is
automatically enabled by select this device type. If you
want this feature disabled, select the g22v10 device type.
%%
%%
%G22V10
G22V10 Architecture
Mnemonic: G22V10 PLCC Mnemonic: YES (G22V10LCC)
DIP Pin Count: 24 Total Product Terms: 132
Extensions: OE D AR SP Pin Controlled Power Down: NO
___________________________________________________________
Manufacturer Device Name
------------ -----------
ATMEL ATF22V10B/L
ATMEL ATF22V10BQ/BQL
Clock Pin(s): 1 Common OE(s):
VCC(s): 24 GND(s): 12
Input Only: 1 2 3 4 5 6 7 8 9 10 11 13
Output Only:
Input/Output: 14 15 16 17 18 19 20 21 22 23
%%